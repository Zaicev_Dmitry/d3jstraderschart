function Validate() {

    function formValidate(form) {
        var isValide = true;

        $('input', form).each(function () {
            var container = $(this).parents('.row-fluid'),
                value = $(this).val();

            if ($(this).attr('required') == 'required' && value.length < 2) {
                $('.error-icon', container).show();
                isValide = false;
            }

            if ($(this).attr('type') == 'email' && value && !isEmail(value)) {
                $('.error-icon', container).show();
                isValide = false;
            }
        });

        return isValide;
    }

    function showMessage(message) {
        var errorParagraph, errorSpan;

        var modal = $('#errorModal');

        $('#errorModal .modal-body').text('');
        if (message && message.length) {
            errorParagraph = $("<p></p>").attr('class', 'text-danger error-message');
            errorSpan = $("<span style='border-bottom: 2px solid #fc1d1d;'></span>")
            errorSpan.text(message);
            errorParagraph.html(errorSpan);
            modal.find('.modal-body').append(errorParagraph);
        }

        if ($('#errorModal .modal-body').html().length) {
            modal.modal();
        }
    }

    function showNoResultMessage(message) {
        var errorParagraph, errorSpan;
        var modal = $('#errorModal');
        modal.find('.modal-title').text('Information');
        if (message && message.length) {
            errorParagraph = $("<p></p>").attr('class', 'text-danger error-message');
            errorSpan = $("<span style='border-bottom: 2px solid #fc1d1d;'></span>")
            errorParagraph.html('');
            errorSpan.text(message);
            errorParagraph.html(errorSpan);
            modal.find('.modal-body').html(errorParagraph);
        }

        if ($('#errorModal .modal-body').length) {
            modal.modal({backdrop: 'static', keyboard: false});
            modal.show();
            $('#errorModal .modal-dialog .modal-content .err').css('display', 'none');
        }
    }

    function showSuccessMessage(message) {
        var successParagraph, successSpan;
        var modal = $('#successModal');
        var body = modal.find('.modal-body').html('');
        if (message && message.length) {
            successParagraph = $("<p></p>").attr('class', 'text-danger success-message');
            successSpan = $("<span style='border-bottom: 2px solid #61ff30;'></span>")
            successSpan.text(message);
            successParagraph.html(successSpan);
            body.append(successParagraph);
        }

        if ($('#successModal .modal-body').html().length) {
            modal.modal();
        }
    }


    function showInformationMessage(message) {
        var informationParagraph, informationSpan;
        var modal = $('#informationModal');
        var body = modal.find('.modal-body').html('');
        if (message && message.length) {
            informationParagraph = $("<p></p>").attr('class', 'text-danger success-message');
            informationSpan = $("<span style='border-bottom: 2px solid #61ff30;'></span>")
            informationSpan.text(message);
            informationParagraph.html(informationSpan);
            body.append(informationParagraph);
        }

        if ($('#informationModal .modal-body').html().length) {
            modal.modal();
        }
    }

    function detectCardType(number) {
        var re = {
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5][0-9]{14}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
        };
        if (re.visa.test(number)) {
            return 'VISA';
        } else if (re.mastercard.test(number)) {
            return 'MASTERCARD';
        } else if (re.diners.test(number)) {
            return 'DINERS';
        } else {
            return false;
        }
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    return {
        formValidate: formValidate,
        detectCardType: detectCardType,
        showMessage: showMessage,
        showNoResultMessage: showNoResultMessage,
        showInformationMessage: showInformationMessage,
        showSuccessMessage: showSuccessMessage
    };
}
