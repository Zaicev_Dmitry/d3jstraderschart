var utils = (function () {
    var parameters;
    var module = {};

    module.getUrlParameter = function (name, defaultValue) {
        if (!parameters) {
            parameters = {};
            var params = window.location.search.substr(1, window.location.search.length - 1).split("&");
            for (var i = 0; i < params.length; i++) {
                var param = params[i].split("=");
                parameters[param[0]] = param[1];
            }
        }
        return parameters[name] ? parameters[name] : defaultValue;
    };

    module.checkServerAvailability = function (onSuccess, onError) {
        // Display loading window.
        FXChart.hideErrorMessage();
        FXChart.showChartLoading();

        $.ajax({
            url: apiUrl + '/serverinfo/version',
            type: 'GET',
            timeout: 5000,
            success: onSuccess,
            error: onError
        });
    };

    module.initializeInformationSection = function () {
        // hide info-section if social url parameter is false
        if (!window.isSocial) {
            $('.game-section').css("right", 0);
            $('.info-section').hide();
            $("a.wins").hide();
            return;
        }

        $('a.wins > .cross-icon').hide(0);
        $('a.wins, .user-section > .restricted a.list-icon, .user-section > .restricted .my-photo').on('click', function () {
            if ($('.game-section').css("right") === "0px") {  // minimized view
                $('a.wins > .trophy-icon').hide(0, function () {
                    $('a.wins > .cross-icon').css("display", "inline-block");
                });
                $('.social-circle').hide();

                // animate info-section
                $('.game-section').animate({right: '187px'}, {queue: false});
                $('.info-section').animate({width: 'toggle'},
                    {
                        queue: false,
                        complete: function () {
                            $('.info-section > .column-invitation > *').show(0);
                            $('.info-section > .column-latest-winners > *').show(0);
                        }
                    });
            } else {  // standard view
                $('a.wins > .cross-icon').hide(0, function () {
                    $('a.wins > .trophy-icon').css("display", "inline-block");
                });
                $('.social-circle').show();

                // animate info-section
                $('.game-section').animate({right: '0'}, {
                    queue: false,
                    complete: function () {
                        $('.game-section').css("right", "");
                    }
                });

                $('.info-section > .column-invitation > *').css("display", "");
                $('.info-section > .column-latest-winners > *').css("display", "");
                $('.info-section').animate({width: 'toggle'},
                    {
                        queue: false,
                        complete: function () {
                            $('.info-section').css("display", "");
                        }
                    });
            }
        });
    };

    module.updateLeftBar = function () {
        $('.game-section').attr("style", "");
        $('.user-section > .expanded').attr("style", "");
        $('.user-section > .anonymous').hide(0);

        if (!window.isSocial) {
            $('.game-section').css("right", 0);
        }
    };

    module.initializeTradersChoiceCheckbox = function () {
        $('.random-percent').css('visibility', 'hidden');
    };

    module.initializeSlimScroll = function () {
        // TODO: use this scroll later
        $('.column-currency .slim-scroll').slimScroll({
            color: "#222222",
            height: '100%'
        });

        $('#inner-trades-wrapper').slimScroll({
            color: "#222222",
            height: '100%'
        });
    };

    module.updateBalanceField = function (balance) {
        $('#balance').text(balance);
    };

    module.isSmallScreen = function () {
        return $(window).height() <= 600;
    };

    module.initializeDepositButton = function () {
        if (!window.isSocial) {
            $('#depositButton').hide();
        } else {
            $('#depositButton').on('click', function () {
                userClass.openUserDepositModal();
            });
        }
    };

    module.initializeTutorialButton = function () {
        $('.btn.show-tutorial').on('click', function () {
            $('#welcomeModal').modal();
            $('#tutorialModal').modal();
        });

        $('.help').parent().on('click', function () {
            $('#tutorialModal').modal();
        });

    };

    module.initializeAboutButton = function () {
        $("a.about").on('click', function () {
            $("#aboutModal").modal();
        });
    };

    return module;
})();