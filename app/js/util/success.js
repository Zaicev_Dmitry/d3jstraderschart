var success = function () {
    var module = {},
        debugMode = false;

    module.showSuccessPopup = function (message) {
        if (!debugMode) {
            return;
        }
        var successParagraph, successSpan;
        var modal = $('#successModal');

        if (message && message.length) {
            successParagraph = $("<p></p>").attr('class', 'text-danger error-message');
            successSpan = $("<span style='border-bottom: 2px solid #61ff30;'></span>")
            successSpan.text(message);
            successParagraph.html(successSpan);
            modal.find('.modal-body').append(successParagraph);
        }

        if ($('#successModal .modal-body').html().length) {
            modal.modal();
        }
    };

    module.initSuccessPopup = function (debug) {
        debugMode = (debug === 'true');

        $('#successModal').on('hidden.bs.modal', function (e) {
            $('#successModal .modal-body').html('');
        })
    };

    return module;
}();
