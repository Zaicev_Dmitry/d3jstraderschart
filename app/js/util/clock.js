(function ($) {
    $.fn.clock = function (initialTime) {
        clockObject = {
            settings: $.extend({
                'currentTime': new Date()
            }, initialTime),
            mNames: new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"),
            dNames: new Array("Sunday", "Monday", "Tuesday",
                "Wednesday", "Thursday", "Friday", "Saturday"),
            formatDate: function () {
                var clockDateTimeString = '';
                clockDateTimeString += this.dNames[this.settings.currentTime.getDay()].l();
                clockDateTimeString += ', ';
                clockDateTimeString += this.settings.currentTime.getDate();
                clockDateTimeString += ' ';
                clockDateTimeString += this.mNames[this.settings.currentTime.getMonth()].l();
                clockDateTimeString += ' ';
                clockDateTimeString += this.settings.currentTime.getFullYear();
                clockDateTimeString += ' ';
                clockDateTimeString += this.settings.currentTime.getHours();
                clockDateTimeString += ':';
                clockDateTimeString += this.checkTime(this.settings.currentTime.getMinutes());
                clockDateTimeString += ':';
                clockDateTimeString += this.checkTime(this.settings.currentTime.getSeconds());
                return clockDateTimeString;
            },
            checkTime: function (i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            },
            updateClock: function () {
                this.settings.currentTime = new Date();
                return this.formatDate();
            }
        };
        return clockObject.updateClock();
    };
})(jQuery);