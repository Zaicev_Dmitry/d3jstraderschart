var error = function () {
    var module = {},
        debugMode = false;

    module.showErrorPopup = function (message) {
        if (!debugMode) {
            return;
        }
        var errorParagraph, errorSpan;
        var modal = $('#errorModal');

        if (message && message.length) {
            errorParagraph = $("<p></p>").attr('class', 'text-danger error-message');
            errorSpan = $("<span style='border-bottom: 2px solid #fc1d1d;'></span>");
            errorSpan.text(message);
            errorParagraph.html(errorSpan);
            modal.find('.modal-body').append(errorParagraph);
        }

        if ($('#errorModal .modal-body').html().length) {
            modal.modal();
        }
    };

    module.initErrorPopup = function (debug) {
        debugMode = (debug === 'true');

        $('#errorModal').on('hidden.bs.modal', function (e) {
            $('#errorModal .modal-body').html("");
        })
    };

    return module;
}();
