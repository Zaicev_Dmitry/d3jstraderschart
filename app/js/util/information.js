var information = function () {
    var module = {},
        debugMode = false;

    module.showInformPopup = function (message) {
        if (!debugMode) {
            return;
        }
        var informationParagraph, informationSpan;
        var modal = $('#informationModal');

        if (message && message.length) {
            informationParagraph = $("<p></p>").attr('class', 'text-danger error-message');
            informationSpan = $("<span style='border-bottom: 2px solid #ecbb38;'></span>")
            informationSpan.text(message);
            informationParagraph.html(informationSpan);
            modal.find('.modal-body').append(informationParagraph);
        }

        if ($('#informationModal .modal-body').html().length) {
            modal.modal();
        }
    };

    module.initInformPopup = function (debug) {
        debugMode = (debug === 'true');

        $('#informationModal').on('hidden.bs.modal', function (e) {
            $('#informationModal .modal-body').html('');
        })
    };

    return module;
}();
