$(document).ready(function () {
    formController();

    $(document).on('click', '#dropdownMenu1', function () {
        console.log('start');
    });

    $(document).on('change, input', '.card div input', function () {
        var number = $(this).val();
        if (number.length > 3) {
            if (!$(this).hasClass('last')) {
                $(this).next().focus();
            }
        }
    });

    $(document).on('keyup, keypress, keydown', '.card div input', function (e) {
        var number = $(this).val();
        if ((e.keyCode == 46 || e.keyCode == 8) && number.length == 0) {
            if (!$(this).hasClass('first')) {
                $(this).prev().focus();
            }
        }
    });

    $(document).on('change, input', '.expire  input', function () {
        var number = $(this).val();
        if (number.length > 1) {
            if (!$(this).hasClass('expiresYear')) {
                $(this).next().focus();
            }
        }
    });

    $(document).on('keyup, keypress, keydown', '.expire input', function (e) {
        var number = $(this).val();
        if ((e.keyCode == 46 || e.keyCode == 8) && number.length == 0) {
            if (!$(this).hasClass('expiresMonth')) {
                $(this).prev().focus();
            }
        }
    });

    $(document).on('change, input', '.card div input:first-child', function () {
        var number = $(this).val();

        var mathPower = Math.pow(10, 16 - number.length);
        var mathPowerDiners = Math.pow(10, 14 - number.length);
        if (number.length > 0) {
            $.cardTupe = validateClass.detectCardType(number * mathPower);
            $.cardTupeDiners = validateClass.detectCardType(number * mathPowerDiners);

            if ($.cardTupe == 'MASTERCARD') {
                $('.visa-mastercard', '#depositModal, #videoDepositModal').removeClass('active');
                $('.master', '#depositModal, #videoDepositModal').addClass('active');
                $('.card-error', '#depositModal, #videoDepositModal').text('');
                $('.card div input.last').attr('maxlength', 4);
            }

            if ($.cardTupe == 'VISA') {
                $('.visa-mastercard', '#depositModal, #videoDepositModal').removeClass('active');
                $('.visa', '#depositModal, #videoDepositModal').addClass('active');
                $('.card-error', '#depositModal, #videoDepositModal').text('');
                $('.card div input.last').attr('maxlength', 4);
            }

            if ($.cardTupeDiners == 'DINERS') {
                $('.visa-mastercard', '#depositModal, #videoDepositModal').removeClass('active');
                $('.dinners', '#depositModal, #videoDepositModal').addClass('active');
                $('.card-error', '#depositModal, #videoDepositModal').text('');
                $('.card div input.last').attr('maxlength', 2);
            }

            if (!$.cardTupe && !$.cardTupeDiners) {
                $('#depositModal .visa-mastercard', '#depositModal, #videoDepositModal').removeClass('active');
                $('#depositModal .card-error', '#depositModal, #videoDepositModal').text("Card type isn't valid");
            } else {
                $('.card-error', '#depositModal, #videoDepositModal').text('');
            }
        }


    });
});

function clearInput(form) {
    $('input,select', form).each(function () {
        $(this).val('');
    });
}

function clearError(form) {

    $('p.error-message', form).each(function () {
        $(this).text('');
    });
}
function formController() {

    $('input')
        .unbind('keypress')
        .bind('keypress', function (event) {
            hideError($(this));
        });

    $('#depositModal button[type="button"]').click(function (event) {
        event.preventDefault();
        $('button').removeClass('active');
        $('input[type="hidden"]', '#depositModal').each(function () {
            $(this).val('');
        });
        $(this).addClass('active');
        $(this).find('input[type="hidden"]').val('1');
    });


    $('#videoDepositModal button[type="button"]').click(function (event) {
        event.preventDefault();
        $('button').removeClass('active');
        $('input[type="hidden"]', '#videoDepositModal').each(function () {
            $(this).val('');
        });
        $(this).addClass('active');
        $(this).find('input[type="hidden"]').val('1');
    });


    $('select')
        .unbind('change')
        .bind('change', function (event) {
            hideError($(this));
            var form = $(this).parents('form');
        });

    $(".new-card").unbind('click')
        .bind('click', function (event) {
            event.preventDefault();
            $('#new-card').css('display', 'block');
            $('#exist-card').css('display', 'none');
            $('#new-card p.error-message').text('');
            $('#depositModal .modal-content').removeAttr('style');
            $('#depositModal .modal-dialog').removeAttr('style');
        });

    $(".exist-card").unbind('click')
        .bind('click', function (event) {
            event.preventDefault();
            $('#exist-card').css('display', 'block');
            $('#new-card').css('display', 'none');
            $('#exist-card p.error-message').text('');
            $('#depositModal .modal-dialog').css('width', '600px', '!important');
            $('#depositModal .modal-dialog .modal-content').css('min-width', '600px', '!important');
        });

    $('#depositModal').on('hidden.bs.modal', function () {
        $('#depositModal .spinner-container').css('display', 'none');
        clearError('#depositModal');
        clearInput('#depositModal')
    });

    $('#withdrawModal button.withdraw').on('click', function () {
        userClass.withdrawalsUser($('#withdrawModal .block'));
    });


    $('#videoDepositModal').on('hidden.bs.modal', function () {
        clearError('#videoDepositModal');
        $('#videoDepositModal .spinner-container').css('display', 'none');
        userClass.stopVideo();
        if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
            console.log('mobile');
            $('body').css('width', '1230px');
        }


        if ($.cookie('welcom') == 'new') {
            $('#welcomeModal').modal();
            $.cookie('welcom', null);
        }
        clearInput('#videoDepositModal')
    });

    $('#withdrawModal').on('hidden.bs.modal', function () {
        $('#withdrawModal .spinner-container').css('display', 'none');
        clearError('#withdrawModal');
        clearInput('#withdrawModal');
    });

    if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
        $('#videoDepositModal').on('show.bs.modal', function () {
            // Also dirty, but we need to tap into the backdrop after Boostrap
            // positions it but before transitions finish.
            $('body').css({
                position: 'fixed',
                width: 'auto',
                top: 0,
                left: 0,
                right: 0
            });
        });
    } else {
        $('#videoDepositModal').on('shown.bs.modal', function () {
            $('#preloader-on').css('display', 'none');
        });
    }


    $('#tutorialModal').on('shown.bs.modal', function () {
        var modal = $('#tutorialModal .modal-dialog')
        var height = modal.actual('outerHeight');
        var width = modal.actual('width');
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        var hRatio = windowHeight / height;
        var vRatio = windowWidth / width;

        var height_result = height * 0.9 * vRatio;
        if (vRatio <= hRatio) {
            modal.attr('style', height_result != 0 ? 'height:' + height * 0.9 * vRatio + 'px!important;' : '' + ' width:' + width * 0.9 * vRatio + '!important;');
        }
        else {
            modal.attr('style', 'width:' + width * 0.9 * hRatio + 'px!important; height:' + height * 0.9 * hRatio + '!important;');
        }
    });

    $('#successModal').on('hidden.bs.modal', function () {
        if ($.cookie('option') == 'get') {
            $('#preloader-on').css('display', 'block');
            FXChart.getOption($.subscribers);
        }
    });

    $('#videoDepositModal #close_deposit').on('click', function () {
        if ($.cookie('option') == 'get') {
            $('#preloader-on').css('display', 'block');
            FXChart.getOption($.subscribers);
        }
    });

    $('#errorModal').on('hidden.bs.modal', function () {
        clearError('#errorModal');
        $('#errorModal').find('.modal-title').text('Error!');
        $('#errorModal .modal-dialog .modal-content .err').css('display', 'block');
    });

    $('#welcomeModal').on('hidden.bs.modal', function () {
        $.cookie('welcom', null);
        $.cookie('index', null);
    });

    $('#profileModal').on('hidden.bs.modal', function () {
        clearError('#profileModal');
    });
    $('#positionsModal').on('hidden.bs.modal', function () {
        $('.presentation').removeClass('active');
    });

    $('#informationModal').on('hidden.bs.modal', function (e) {
        $('#informationModal .modal-body').html('');
    });

    $('select[name="reg_country"]')
        .unbind('change')
        .bind('change', function (event) {
            event.preventDefault();
            userClass.phoneCode($(this).val());
        });

}

function userFormController() {
    $('#registrationModal form')
        .unbind('submit')
        .bind('submit', function (event) {
            event.preventDefault();
            $('p.error-message', $(this)).hide();
            userClass.registrationUser($(this));
        });

    $('#depositModal form')
        .unbind('submit')
        .bind('submit', function (event) {
            event.preventDefault();
            $('p.error-message', $(this)).hide();
            $('.error-icon').hide();
            userClass.depositUser($(this));
        });

    $('#videoDepositModal form')
        .unbind('submit')
        .bind('submit', function (event) {
            event.preventDefault();
            $('p.error-message', $(this)).hide();
            $('.error-icon').hide();
            userClass.depositUser($('#videoDepositModal form'));
        });

    $('#welcomeModal .show-tutorial').bind('click', function () {
        $('#tutorialModal').modal();
    });

    $('form.internal-login-form').unbind('submit')
        .bind('submit', function (event) {
            event.preventDefault();
            $('p.error-message', $(this)).hide();
            userClass.loginUser($(this));
        });

    $('form.internal-profile-form').unbind('submit')
        .bind('submit', function (event) {
            event.preventDefault();
            $('p.error-message', $(this)).hide();
            userClass.resetUserPassword('.internal-profile-form');
        });
    formController();

    $('#registrationModal select[name="country"]').change(function () {
        $('#registrationModal input[name="reg_country"]').val($(this).val());
    });
}


function hideError(element) {
    var container = element.parents('.row-fluid');

    $('.error-icon', container).hide();
}

function menuController() {
    $('.positions').parent().on('click', function (event) {
        event.preventDefault();
        $('#positionsModal .nav.nav-tabs li').removeClass('active');
        $('#positionsModal .pagination').html('');
        $('#positionsModal #open .table tbody').html('');
        $('#positionsModal #won  .table tbody').html('');
        $('#positionsModal #lost .table tbody').html('');
        $('#positionsModal #tabsPositions').removeClass('active');
        $('#positionsModal #open_position').parent().addClass('active');

        $('#positionsModal').modal();

        var container = $('#positionsModal .active');
        var sniper = container.find('a .spinner-container').css('display', 'block');

        positionsClass.getPositionsForPagination(1, 'open', sniper, 0, 25);

        $(document).on('click', '#positionsModal #prev', function () {
            var prev = $('span.pagination-button.number.active').prev();
            prev.click();
        });

        $(document).on('click', '#positionsModal #next', function () {
            var next = $('span.pagination-button.number.active').next();
            next.click();
        });

        $(document).on('click', '#positionsModal #last', function () {
            $('span.pagination-button.number.last').click();
        });

        $(document).on('click', '#positionsModal #first', function () {
            $('span.pagination-button.number.first').click();

        });


        $(document).on('click', 'span.pagination-button.number', function () {
            var container = $('#positionsModal .active');
            var sniper = container.find('a .spinner-container').css('display', 'block');
            var status = container.children().attr('aria-controls');
            var page = $(this).attr('page');
            var first_result = (parseInt(page) * 10) - 10;
            $('span.pagination-button.number').removeClass('active');
            positionsClass.getPositionsForPagination(parseInt(page), status, sniper, first_result, 10);
        });

        $('#positionsModal .nav-tabs li')
            .unbind('click')
            .bind('click', function () {
                $('#positionsModal .pagination').html('');
                var statusPositions = $('a', this).attr('aria-controls');

                var spinner = $('a', this).children();
                spinner.css('display', 'block');
                positionsClass.getPositionsForPagination(1, statusPositions, spinner, 0, 10);
            });
    });

    $('.account').parent().on('click', function (event) {
        event.preventDefault();
        $('#profileModal').modal();
        userClass.showUserDataAtPopup();
    });

    $('.help').parent().on('click', function () {
        $('#tutorialModal').modal();
    });


    $('.withdrawals').parent().on('click', function (event) {
        event.preventDefault();
        $('#withdrawModal').modal();
        userClass.getCardsUser();
        userClass.getWithdrawalsUser();

        $('#withdrawModal .nav-tabs li')
            .unbind('click')
            .bind('click', function () {

            });
    });
}