var FXChart = (function () {
    var chart;
    var count = 0;
    var countDecimal = 0;
    var allCurrencyList = [];
    var currentCurrency;
    var startPoint = 0;
    var point = [];
    var minPoint = [],
        maxPoint = [],
        delta = 0;
    var firstPoint = 0;
    var integrate = new Integrate();
    var chartSettings = {
        cxPointer: 0,
        cxTooltip: 0,
        cyTooltip: 0,
        cyPointer: 0,
        minX: -60,
        maxX: 0,
        lastX: 70,
        minY: 0,
        maxY: 0,
        chartPoints: {},
        currentPoint: {currency: 0, timestamp: 0},
        prizeMatrix: {},
        bets: {},
        pips: [],
        resize: false
    };
    var data = [{
        values: [],
        key: 'Currency',
        color: '#000000'
    }];
    var maxTradeMeAmount = 9;


    var timeout,
        cleaningFlag = true,
        lastCleanTime = 0,
        latestTradesIntervals = {},
        updateStatusIntervals = {},
        loadingBet = {};

    $(document).bind('select.input-investment', 'change', function () {
            updateBets();
        }
    );

    function currentInvest() {
        $('.cub').each(function () {
            if ($('.input-investment').val() == $(this).find('.invest').attr('val')) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

    $(document).on('click', '.cub', function () {
        var $inputInvestment = $('.input-investment');
        var amount = $inputInvestment.val();
        var balance = $('#balance').text();
        $.currentAmount = $(this).find('.invest').attr('val');
        if (parseInt(amount) <= parseInt(balance)) {
            $inputInvestment.val($(this).find('.invest').attr('val'));
            currentInvest();
        } else {
            userClass.openUserDepositModal();
            return false;
        }
    });


    function checkBrockerBets30(brocker) {
        switch (brocker) {
            case 'INSO':
                return false;

            case 'SECO':
                return false;

            case 'PWR':
                return false;

            case 'BDBE':
                return false;

            default:
                return true;
        }
    }


    /**
     * set decimal number count
     * @array currencyList
     */
    function setDecimal(assetsList) {
        $.each(assetsList, function (index, currency) {
            var data = ('' + currency[1]).split('.');
            countDecimal = Math.max(countDecimal, data[1] ? data[1].length : 0);
        });
    }

    /**
     * init graphp from asset
     */
    function initHistoryData() {
        // Game was started already, so we set restart to false.
        restart = false;

        // Clean values (e.g. for new instrument).
        chartSettings.chartPoints = data[0];
        chartSettings.chartPoints.values = [];
        chartSettings.currentInstrument = currentInstrument || currencyText;


        jQuery.getJSON(spotPlatform + '/PlatformAjax/getJsonFile/platformGraph/regular/binary/-1minutes/now/asset/' + defaultCurrencyID + '/graphData.spotgraph',
            function (responseData) {

                var returnedData = responseData.data.graph[defaultCurrencyID].series[0].data,
                    parseData = [];

                setDecimal(returnedData);
                parseData.push({
                    instrument: currencyText,
                    currency: returnedData[returnedData.length - 1][1],
                    timestamp: returnedData[returnedData.length - 1][0]
                });
                lastItemTime = parseData[parseData.length - 1].timestamp;
                currentTradeValue = parseData[parseData.length - 1].currency;
                chartSettings.chartPoints.values = parseData;

                var responsePrize = '{"30":{"0.1":2.1,"-0.1":1.5},"60":{"0.1":1.4,"-0.1":1.6}}';
                hideChartLoading();
                hideErrorMessage();
                $('#chart').show();

                chartSettings.prizeMatrix = JSON.parse(responsePrize);

                // Initialize pips values.
                var level = 1;
                for (var i in chartSettings.prizeMatrix[30]) {
                    chartSettings.pips[level] = i;
                    level++;
                }

                chartSettings.currentPoint = chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 1];
                chart = createInstance(chartSettings.chartPoints.values);

            }
        );
    }

    function updateChartSettings() {
        var currentPoint = chartSettings.currentPoint.currency;
        // Extract min and max currency relative start point and increment they to add chart 'padding'.
        // Hardcoded for EUR/USD(0.0002).
        if (chartSettings.currentPoint) {
            if (startPoint == 0) {
                startPoint = currentPoint;
                point.push(currentPoint);
            }

            if (point[0] == currentPoint) {
                point.push(currentPoint);
            } else {
                point = [];
                startPoint = 0;
            }

            if (firstPoint == 0) {
                firstPoint = currentPoint;
                delta = firstPoint / 2000 * 1.05;
                chartSettings.minY = firstPoint - delta;
                chartSettings.maxY = firstPoint + delta;
            }

            chartSettings.minY = chartSettings.currentPoint.currency - delta;
            chartSettings.maxY = chartSettings.currentPoint.currency + delta;

        }

        var formatDecimal = countDecimal > 0 ? countDecimal : 1;
        chartSettings.formatedCurrency = d3.format('.' + formatDecimal + 'f')(chartSettings.currentPoint.currency);
    }

    function buildYAxis() {
        var ticks;
        if (utils.isSmallScreen()) {
            ticks = 6;
        }
        else {
            ticks = 10;
        }

        var formatDecimal = countDecimal > 0 ? countDecimal : 1;
        var y = d3.scale
            .linear()
            .domain([chartSettings.minY, chartSettings.maxY]);
        chart.yAxis
            .tickValues(y.ticks(ticks))
            .tickFormat(d3.format('.0' + formatDecimal + 'f'));
        chart.forceY([chartSettings.minY, chartSettings.maxY])
            .yDomain([chartSettings.minY, chartSettings.maxY]);
    }

    function buildXAxis() {
        var ticks = [];
        for (var i = -50; i < 80; i += 10) {
            ticks.push(i);
        }
        chart.xAxis
            .tickFormat(function (d) {
                var sign = "";
                if (d < 0) {
                    sign = "-";
                }
                return sign.concat(0, d / 60 >> 0, ":", Math.abs(d % 60 / 10), 0);
            })
            .tickValues(ticks);
    }

    /**
     * show investment bar
     */
    function buildInvestmentBar() {
        var select = '<select>';
        var investmentValue = currentCurrency.rule.amountRange.split(',');
        $.each(investmentValue, function (index, value) {

            var selcrted = $.currentAmount == value ? 'selected' : '';
            select += '<option value="' + value + '"' + selcrted + '>' + value + '</option>';
        });
        select += '</select>';
        var input = $(select)
            .attr('class', 'input-investment')
            .on('change', function () {
                var amount = $('.input-investment').val();
                var balance = $('#balance').text();

                $.currentAmount = amount;
                if (parseInt(amount) >= parseInt(balance)) {
                    userClass.openUserDepositModal();
                    return false;
                } else {
                    updateBets();
                }
            });
        currentInstrument = currentInstrument ? currentInstrument : currencyText;

        $('#chart')
            .prepend(
                "<div class='current-currency-investment'>" +
                "<strong class='currency'>$</strong>" +
                "</div>");

        $('.current-currency-investment').append(input);
        $('.current-currency-investment').append("<span>" + 'Investment'.l() + "</span>");
        buildInvestmentBlock();
    }

    function buildInvestmentBlock() {

        var div = '<div class="block_invest">';
        var investmentValue = currentCurrency.rule.amountRange.split(',');
        $.each(investmentValue, function (index, value) {
            div += '<div class="cub"><strong class="currency">$ </strong><span class="invest"  val="' + value + '">' + value + '</span></div>';
        });
        div += '</div>';
        $('#chart')
            .prepend(
                "<div class='current-currency-box'>" +
                "<div class='new-box'>" +
                "<div class='current-currency-name'>" + currentInstrument + "</div>" +
                "<div class='current-currency-value'>" + chartSettings.formatedCurrency + "</div>" +
                "</div>" +
                "</div>" +
                "<div class='investment'>" +
                "<span class='invest-text'>" + 'Current Investment'.l() + "</span>" +
                "</div>");

        $('.investment').append(div);
    }

    function updateInvestment() {
        var previousPoint = chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 2]
        if (!chartSettings.currentPoint || !previousPoint) {
            return;
        }

        if (chartSettings.currentPoint.currency < previousPoint.currency) {
            $(".current-currency-value")
                .text(chartSettings.formatedCurrency)
                .attr('class', 'current-currency-value currency-down');
            $(".current-instrument-regulation")
                .removeClass("up")
                .addClass("down");
        }
        else if (chartSettings.currentPoint.currency > previousPoint.currency) {
            $(".current-currency-value")
                .text(chartSettings.formatedCurrency)
                .attr('class', 'current-currency-value currency-up');
            $(".current-instrument-regulation")
                .removeClass("down")
                .addClass("up");
        }
        else {
            $(".current-currency-value")
                .text(chartSettings.formatedCurrency)
                .attr('class', 'current-currency-value currency-same');
            $(".current-instrument-regulation")
                .removeClass("down")
                .removeClass("up");
        }
    }

    function calculateBetsCoords() {
        var transform30 = '', transform60 = '';
        $('.nv-x.nv-axis .tick').each(function () {
            var text = $(this).find('text').text();
            if (text == '00:30') {
                transform30 = $(this).attr('transform');
            }
            if (text == '01:00') {
                transform60 = $(this).attr('transform');
            }
        });
        cxB = {
            30: transform30.substr(10, 5),
            60: transform60.substr(10, 5)
        };
        cyB = 149;
        var graphHeight = parseInt($('.nv-lineChart > g > rect').attr('height')),
            halfGraphHeight = graphHeight / 2;
        levels = [30, 60];

        chartSettings.bets.coord = {};
        chartSettings.bets.prizeIndex = {};
        chartSettings.bets.color = {};
        for (var level in levels) {
            var l = 1.5;
            level = levels[level];
            chartSettings.bets.coord[level] = {};
            chartSettings.bets.prizeIndex[level] = {};
            chartSettings.bets.color[level] = {};

            for (var i = 1; i < 3; i++) {
                chartSettings.bets['coord'][level][i] = {};
                chartSettings.bets['coord'][level][i]['x'] = Number(cxB[level].replace(/,/, "."));

                if (i === 1) {
                    chartSettings.bets['coord'][level][i]['y'] = halfGraphHeight - 191;
                    chartSettings.bets['prizeIndex'][level][i] = 0.1;
                    l = 0.0;
                }
                if (i === 2) {
                    chartSettings.bets['coord'][level][i]['y'] = halfGraphHeight + 6;
                    chartSettings.bets['prizeIndex'][level][i] = -0.1;
                    l = 0.0;
                }

            }
        }
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function isMobile() {
        return (
            //Detect iPhone
            (navigator.platform.indexOf("iPhone") != -1) ||
            //Detect iPod
            (navigator.platform.indexOf("iPod") != -1) ||
            // detect iPad
            (navigator.platform.indexOf("iPad") != -1)
        );
    }

    /**
     * insert bets at graph
     */
    function buildBets() {
        calculateBetsCoords();
        d3.select('.nv-lineChart g')
            .insert("g", '.nv-linesWrap')
            .attr("class", "bets");

        var levels = [30, 60],
            amount = $('.input-investment').val(),
            currencyText = userClass.getUserCurrency(),
            y_line,
            classBet,
            classPath = $(".nv-scatter").attr('class').split(' ').pop().split('-').pop();

        positionsClass.setAssetId(defaultCurrencyID);
        positionsClass.setAmount(amount);
        levels.forEach(function (level) {
            var profitValue = level == 30 ? currentCurrency.profitMultiplierThirty : currentCurrency.profitMultiplierSixty,
                profit = profitValue / 100 + 1;
            var checkBroker = checkBrockerBets30(userData.broker);
            for (var i = 1; i < 3; i++) {
                classBet = i <= 1 ? 'call' : 'put';
                var visibility = 'visible';
                //TODO now 30 bets are disabled
                if (!checkBroker && level == 30) {
                    visibility = 'hidden';
                }
                var translateY = chartSettings.bets['coord'][level][i]['y'],
                    translateX = chartSettings.bets['coord'][level][i]['x'];
                var nvLineChart = d3.select('.bets')
                    .append("g")
                    .style("clip-path", "url(#nv-edge-clip-" + classPath + ")")
                    .append("g")
                    .attr('order', i)
                    .attr('level', level)
                    .attr('id', 'id' + level + i)
                    .attr('class', classBet + ' bet current-currency-bet' + level + '-' + i)
                    .attr('transform', "translate(" + translateX + "," + translateY + ")")
                    .style('visibility', visibility)
                    .on("click", function () {
                        rateButtonClick($(this))
                    });
                if (!isMobile()) {
                    nvLineChart
                        .on("mouseenter", addBetHighlight)
                        .on("mouseleave", removeBetHighlight);
                }
                nvLineChart
                    .append("image")
                    .attr('x', -60)
                    .attr('y', 0)
                    .attr('height', 185)
                    .attr('rx', 3)
                    .attr('ry', 3)
                    .attr('width', 120)
                    .attr("xlink:href", (i <= 1 ? "img/new/block-up.png" : "img/new/block-down.png"));
                nvLineChart
                    .append("text")
                    .attr('class', 'bet-text-title')
                    .attr('y', 90)
                    .attr('x', 2)
                    .text("PAYOUT");
                nvLineChart
                    .append("text")
                    .attr('x', 2)
                    .attr('y', 110)
                    .attr('width', 172)
                    .text(currencyText + ' ' + (amount * profit).toFixed(2))
                    .attr('class', 'bet-txt bet-txt' + level + '-' + i);
            }
        });
    }

    function tradersChoiceRandomGenerator(order) {
        if (order === 1 || order === 2) {
            return getRandomInt(20, 30);
        }
    }

    function setPossibleReturn(target) {
        if (target) {
            var level = target.attr('level');
            var order = target.attr('order'),
                amount = $('.input-investment').val();
            var profitValue = level == 30 ? currentCurrency.profitMultiplierThirty : currentCurrency.profitMultiplierSixty,
                profit = profitValue / 100 + 1;

            var possibleReturn = (amount * profit).toFixed(2);
            $('.current-return-regulation').html(possibleReturn);
        }
    }

    function setBetCurrency(target) {
        if (target) {
            var level = target.attr('level');
            var order = target.attr('order');

            if (Number(order) === 1) {
                $('.current-bet-regulation').html(getBetCurrency(order)).addClass('up').removeClass('down');
            }
            if (Number(order) === 0) {
                $('.current-bet-regulation').html(getBetCurrency(order)).addClass('down').removeClass('up');
            }

        }
    }

    function getBetCurrency(order) {
        var currentPoint = chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 1];
        return currentPoint.currency;
    }

    /**
     * View popup window after set bet
     */
    function rateButtonClick(handler) {

        // Set current bet for reg modal updating.
        chartSettings.modalTarget = handler;

        var regPopupCookies = $.cookie("isRegulationModal");
        if (typeof regPopupCookies !== "undefined" && Number(regPopupCookies) === 0) {
            placeBet(chartSettings.modalTarget);
        }
        else {
            $('.current-time-regulation').html(chartSettings.modalTarget.attr('level'));
            $('.current-instrument-regulation').html(currentInstrument);
            $('.current-investment-regulation').html($('.input-investment').val());
            setPossibleReturn(chartSettings.modalTarget);
            setBetCurrency(chartSettings.modalTarget);
            if (chartSettings.modalTarget.attr('order') == 2) {
                $('.current-bet-text-regulation').html('lower than').addClass('down').removeClass('up');
            }
            else if (chartSettings.modalTarget.attr('order') == 1) {
                $('.current-bet-text-regulation').html('higher than').addClass('up').removeClass('down');
            }
            $('#regulationModal').modal('show');

            // Set default checkbox value.
            $('#regulationModal').on('show.bs.modal', function () {
                $('#user-regulation-choice').prop('checked', false);
            });

            $('#regulation-approve-btn').off('click');
            $('#regulation-approve-btn').on("click", function () {
                var date = new Date();
                var minutes = 15;
                date.setTime(date.getTime() + (minutes * 60 * 1000));
                if ($('#user-regulation-choice').is(':checked')) {
                    $.cookie("isRegulationModal", 0, {expires: date});
                }
                else {
                    $.cookie("isRegulationModal", 1, {expires: date});
                }
                placeBet(chartSettings.modalTarget);
            });
        }
    }

    function placeBet(target) {

        var invoker = target;
        var order = invoker.attr('order');
        var button = invoker;
        var uuid = positionsClass.positionsList.length;
        var level = invoker.attr('level');
        var time = chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 1].timestamp + level * 1000;
        var sign, betStatus, currency;
        var position = order <= 1 ? 'call' : 'put';
        var placeBetCallback = function () {
            var amount = $('.input-investment').val();
            var balance = $('#balance').text();

            var profitValue = level == 30 ? currentCurrency.profitMultiplierThirty : currentCurrency.profitMultiplierSixty,
                profit = profitValue / 100 + 1;

            chartSettings.current_rate = (profit * amount);
            data.push({});
            data[data.length - 1].key = uuid;
            data[data.length - 1].invisible = true;
            data[data.length - 1].rate = chartSettings.current_rate;
            data[data.length - 1].investment = chartSettings.prizeCoef;
            data[data.length - 1].instrument = currentInstrument;
            data[data.length - 1].name = makeLetterID();

            $('.bet *').css({opacity: 0.6});
            $('.bet, .bet *').off("click")
                .on("click", function () {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                });


            loadingBet[uuid] = true;

            setTimeout(function () {
                    $('.bet *').css({opacity: 1});
                    loadingBet[uuid] = false;
                    data[data.length - 1].invisible = false;
                    $('.bet *').off("click");
                    $('.bet').on('click', function (event) {
                        event.preventDefault();
                        rateButtonClick($(this));
                    });
                },
                1500);

            setTimeout(function () {
                    button.css('display', 'inherit');
                },
                2500);

            if (Number(order) <= 1) {
                sign = '<i class="fa fa-sort-asc"></i>';
                betStatus = 'up';
                currency = getBetCurrency(order);
                data[data.length - 1].type = 'buy';
                data[data.length - 1].color = '#4aba33';
                data[data.length - 1].values = [{
                    'timestamp': time,
                    'currency': currency
                },
                    {
                        'timestamp': time,
                        'currency': Number(currency) + 100
                    }];
            }
            else if (Number(order) >= 2) {
                sign = '<i class="fa fa-sort-desc"></i>';
                betStatus = 'down';
                currency = getBetCurrency(order);
                data[data.length - 1].type = 'sell';
                data[data.length - 1].color = '#fd3737';
                data[data.length - 1].values = [{
                    'timestamp': time,
                    'currency': currency
                },
                    {
                        'timestamp': time,
                        'currency': 0
                    }];
            }

            if ($('.latest-trades-list li').length >= 6) {
                $('.latest-trades-list li:last-child').remove();
            }
            if (Number(level) === 30) {
                restTime = '00:30';
                secondsCount = 30;
            }
            else {
                restTime = '01:00';
                secondsCount = 60;
            }

            latestTradesIntervals[uuid] = setInterval(function () {
                var tradeRow = $('.latest-trades-list').find('.position' + uuid + ' .latest-trades-time');
                var seconds = Number(tradeRow.attr('rest-time')) - 1;
                tradeRowText = preflightZero2(Math.floor(seconds / 60)) + ':' + preflightZero2(seconds % 60);
                tradeRow.text(tradeRowText);
                tradeRow.attr('rest-time', seconds);

                if (seconds <= 0) {
                    clearInterval(latestTradesIntervals[uuid]);
                }
            }, 1000);
            $('.latest-trades-list')
                .prepend('<li class="position' + uuid + ' live"><span class="latest-trades-instrument">' + currentInstrument + ' ' + Number(currency).toFixed(5) + sign + '</span><span rest-time="' + secondsCount + '" class="latest-trades-time ' + betStatus + '">' + restTime + '</span></li>')
                .show('slow');

            positionsClass.positionsList[uuid] = {};
            redrawInstance(data);
        };

        if (parseInt($('#balance').text()) >= parseInt($('.input-investment').val())) {
            placeBetCallback();

            var errorCallback = function (dataPosition) {
                    if (dataPosition.errors[0].message == "customerSuspended") {
                        $('#errorModal .modal-body').text('');
                        validateClass.showMessage("Please contact " + $.cookie('email_broker') + " to verify your account or wait for a call from " + $.cookie('broker'));
                    } else if (dataPosition.errors[0].message == 'sessionExpired') {
                        userClass.userSessionExpired();
                    } else if (dataPosition.errors[0].message == 'invalidRegulateStatus') {
                        $('#errorModal .modal-body').text('');
                        validateClass.showMessage('Invalid regulate status');

                    } else if (dataPosition.errors[0].localizedMessage.l() != 'The system currently unavailable. Please try later') {
                        $('#errorModal .modal-body').text('');
                        validateClass.showMessage(dataPosition.errors[0].localizedMessage.l());

                    } else if (dataPosition.errors[0].localizedMessage.l() == 'The system currently unavailable. Please try later') {
                        $('#errorModal .modal-body').text('');
                        validateClass.showMessage('Error at work with position');
                    }
                    else {
                        $('#errorModal .modal-body').text('');
                        validateClass.showMessage('Error at work with position');
                    }

                    positionsClass.positionsList[uuid].data_status = 'error';
                    clearTimeout(updateStatusIntervals[uuid]);

                    setTimeout(function () {
                        var errorBetTraders = $('.position' + uuid + '.live.finish');
                        errorBetTraders.remove();
                    }, 1500);

                },
                successCallback = function (dataPosition) {
                    if (dataPosition.status == 'OK') {
                        positionsClass.positionsList[uuid] = dataPosition.Positions[0];
                    } else {
                        errorCallback(dataPosition);
                    }
                };

            positionsClass.openPosition(position, level, successCallback, errorCallback);

            updateStatusIntervals[uuid] = setTimeout(function () {
                positionsClass.getPosition(uuid);
            }, secondsCount * 1000);

            updateStatusIntervals[uuid] = setTimeout(function () {
                positionsClass.getPosition(uuid);
            }, (secondsCount + 10) * 1000);

        } else {
            userClass.openUserDepositModal();
        }
    }

    function addBetHighlight() {
        $('.bet').css('opacity', 0.3);
        $(this).css('opacity', 1).find('rect').attr('width', 88).attr('x', -44);
        $(this).find('text').attr('active', true);
        $(this).find('line').css('visibility', 'visible');
        $(this).find('rect').css('stroke-dasharray', '0 106px 88px 15px');
        if ($(this).attr('id') == 'id301' || $(this).attr('id') == 'id601') {
            $(this).find('image').attr("href", "img/new/block-up_hover.png");
        } else if ($(this).attr('id') == 'id302' || $(this).attr('id') == 'id602') {
            $(this).find('image').attr("href", "img/new/block-down_hover.png");
        }
        updateBets();
    }

    function removeBetHighlight() {
        $('.bet').css('opacity', 1);
        $(this).find('rect').attr('width', 80).attr('x', -40);
        $(this).find('text').attr('active', false);
        $(this).find('line').css('visibility', 'hidden');
        $(this).find('rect').css('stroke-dasharray', '0 98px 80px 15px');
        if ($(this).attr('id') == 'id301' || $(this).attr('id') == 'id601') {
            $(this).find('image').attr("href", "img/new/block-up.png");
        } else if ($(this).attr('id') == 'id302' || $(this).attr('id') == 'id602') {
            $(this).find('image').attr("href", "img/new/block-down.png");
        }
        updateBets();
    }

    function updateBets() {
        chartSettings.prizeCoef = parseInt($('.input-investment').val(), 10);

        if (currentCurrency == undefined) return false;
        var levels = [30, 60],
            currency = userClass.getUserCurrency(),
            amount = $('.input-investment').val();
        var balance = $('#balance').text();
        var first = 0;


        if (parseInt(balance) == 0 && first == 0) {
            first = 1;
        }

        if (first == 0 && parseInt(amount) > parseInt(balance)) {
            return false;
        }

        positionsClass.setAmount(amount);
        levels.forEach(function (level) {

            var profitValue = level == 30 ? currentCurrency.profitMultiplierThirty : currentCurrency.profitMultiplierSixty,
                profit = profitValue / 100 + 1;
            for (var i = 1; i < 3; i++) {

                $('.random-percent-' + level + '-' + i).text(tradersChoiceRandomGenerator(i) + '%');

                if (chartSettings.resize) {
                    calculateBetsCoords();
                    d3.select('.current-currency-bet' + level + '-' + i)
                        .attr('transform', "translate(" + chartSettings.bets['coord'][level][i]['x'] + "," + chartSettings.bets['coord'][level][i]['y'] + ")")
                }
                $('.bet-txt' + level + '-' + i).text(currency + ' ' + (profit * amount).toFixed(2));
            }
        });
    }

    function calculatePointerCoord() {
        nvLineRect = $('.nv-lineChart rect');
        chartSettings.cxPointer = nvLineRect.attr('width') / 2.165;
        chartSettings.cxTooltip = chartSettings.cxPointer + 10;
        chartSettings.cyPointer = nvLineRect.attr('height') / 2;
        chartSettings.cyTooltip = chartSettings.cyPointer;
    }


    function buildPointer() {
        calculatePointerCoord();
        var nvLineChart = d3.select('#chart svg .nv-lineChart');
        nvLineChart
            .append("line")
            .attr('class', 'current-line-dashed')
            .attr('x1', 0)
            .attr('y1', chartSettings.cyPointer)
            .attr('y2', chartSettings.cyPointer)
            .attr('x2', 9999)
            .attr('y2', chartSettings.cyPointer);
        nvLineChart
            .append('polygon')
            .attr('points', '-5,0 0,3 0,10, 55,10, 55,-10 0,-10 0,-3')
            .attr('width', 52)
            .attr('height', 20)
            .attr('fill', 'white')
            .attr('class', 'current-tooltip-rect')
            .attr('transform', "translate(" + chartSettings.cxTooltip + "," + chartSettings.cyTooltip + ")");
        nvLineChart
            .append("text")
            .text(chartSettings.formatedCurrency)
            .attr('class', 'current-tooltip-text')
            .attr('transform', "translate(" + (chartSettings.cxTooltip + 5) + "," + (chartSettings.cyTooltip + 5) + ")")
            .transition().duration(800).ease('cubic-in-out');
        nvLineChart
            .append("circle")
            .attr('class', 'current-pointer-item')
            .attr('cx', chartSettings.cxPointer)
            .attr('cy', chartSettings.cyPointer)
            .attr('r', 4)
            .transition().duration(800).ease('cubic-in-out');
    }

    function updatePointer() {
        if (chartSettings.resizePointer) {
            calculatePointerCoord();
            d3.select('#chart .current-tooltip-rect')
                .text(chartSettings.formatedCurrency)
                .attr('transform', "translate(" + chartSettings.cxTooltip + "," + chartSettings.cyTooltip + ")");
            d3.select('#chart .current-tooltip-text')
                .text(chartSettings.formatedCurrency)
                .attr('transform', "translate(" + (chartSettings.cxTooltip + 5) + "," + (chartSettings.cyTooltip + 5) + ")")
                .transition().duration(800).ease('cubic-in-out');
            d3.select('#chart .current-line-dashed')
                .attr('y1', chartSettings.cyPointer)
                .attr('y2', chartSettings.cyPointer);
            d3.select('#chart .current-pointer-item')
                .attr('cx', chartSettings.cxPointer)
                .attr('cy', chartSettings.cyPointer)
                .transition().duration(800).ease('cubic-in-out');
            chartSettings.resizePointer = false;
        }
        else {
            var $chartForUpdate = $('#chart');
            $chartForUpdate.find('.current-tooltip-rect')
                .text(chartSettings.formatedCurrency);

            $chartForUpdate.find('.current-tooltip-text')
                .text(chartSettings.formatedCurrency);
        }
    }

    function redrawInstance() {
        clearTimeout(timeout);

        if (cleaningFlag) {
            updateChartSettings();
            buildYAxis();

            if (typeof chart !== "undefined" && currentInstrument === chartSettings.currentInstrument) {
                d3.select('#chart svg')
                    .datum(data)
                    .transition()
                    .duration(800)
                    .ease('linear')
                    .call(chart);
            }

            executeRate();
            setBetCurrency(chartSettings.modalTarget);
            updateInvestment();
            updatePointer();
            updateBets();

            cleanChart();
        }

        return chart;
    }

    function cleanChart() {
        var currentTime = chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 1].timestamp;
        if (currentTime - lastCleanTime < 60000) {
            // if since last clean passed less then 1 minute, do nothing
            return;
        }
        lastCleanTime = currentTime;

        // We store history only about last 60 sec.
        cleaningFlag = false;
        var isCleaning = false;
        for (var j = 0; j < data.length; j++) {
            if (j == 0) {
                var length = data[j].values.length;
                var i = 0;
                while (currentTime - data[j].values[i].timestamp > 70000) {
                    i++;
                }
                if (i > 0) {
                    isCleaning = true;
                    data[j].values = data[j].values.slice(1 + i - length);
                }
            }
            else {
                if (currentTime - data[j].values[0].timestamp > 70000) {
                    isCleaning = true;
                    data.splice(j, 1);
                }
            }
        }

        if (isCleaning) {
            console.log("Cleaning");

            if (typeof chart !== "undefined") {
                d3.select('#chart svg')
                    .datum(data)
                    .transition().duration(0)
                    .call(chart);
            }
            d3.select("#chart").transition().each("end", function () {
                cleaningFlag = true;
            });
        }
        else {
            cleaningFlag = true;
        }
    }

    function makeLetterID() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    function executeRate() {
        $.each(data, function (index) {
            var position = positionsClass.positionsList[data[index].key],
                isBetPoint = data[index].type == 'sell' || data[index].type == 'buy';

            if (data[index].history !== true && isBetPoint && position != undefined) {
                var finishPosition = function (className) {
                    var latestTrade = $('.position' + data[index].key);
                    latestTrade.addClass('finish');
                    clearInterval(latestTradesIntervals[data[index].key]);

                    latestTrade.find('.latest-trades-time').text('$' + data[index].rate.toFixed(2)).addClass(className);
                    $('.nv-group.nv-series-' + index + ' .rate')
                        .attr('class', 'rate ' + className);
                    $('.nv-group.nv-series-' + index + ' .tooltip-text-container')
                        .attr('class', 'tooltip-text-container ' + className);
                    data[index].history = true;
                    redrawInstance(data);
                };
                var tempTimestamp = data[index].values[0].timestamp;
                var tempCurrency = data[index].values[0].currency;
                switch (position.data_status) {
                    case 'error':
                        finishPosition('error');
                        data[index].invisible = true;
                        break;
                    case 'won':
                        finishPosition('winner');
                        var dataLength = chartSettings.chartPoints.values.length - 1;
                        tempTimestamp = chartSettings.chartPoints.values[dataLength].timestamp;
                        tempCurrency = chartSettings.chartPoints.values[dataLength].currency;
                        data[index].values = [{
                            'timestamp': tempTimestamp,
                            'currency': tempCurrency
                        }];
                        data[index].color = '#ff6600';
                        data[index].won = true;

                        // Added win circle
                        if (data[index].instrument === currentInstrument) {
                            createWonCircleAnimation(data[index].name, data[index].rate);
                        }

                        break;
                    case 'lost':
                        finishPosition('loser');
                        setTimeout(function () {
                            data[index].values = [{
                                'timestamp': tempTimestamp,
                                'currency': tempCurrency
                            }];
                        }, 1500);

                        data[index].color = '#BBBBBB';
                        data[index].lose = true;

                        break;
                    default :
                        break;
                }

            }

        });

        $('.nv-scatterWrap .nv-group').each(function (index) {
            if (index > 0) {

                $(this).find(".nv-point").attr('idBet', data[index].name);
                var groupClass = $(this).attr('class');
                var arr = groupClass.split(' ');
                if (data[index].invisible) {
                    $('.' + arr[1] + ' .nv-line').css('display', 'none');
                    $(this).find('.circle-rate').css('visibility', 'hidden');
                    $(this).find('.rate').css('visibility', 'hidden');
                } else {
                    $('.' + arr[1] + ' .nv-line').css('display', 'inherit');
                    if (data[index].history !== true) {
                        $(this).find('.circle-rate').css('visibility', 'visible');
                        $(this).find('.rate').css('visibility', 'visible');
                    }
                }
                if (data[index].instrument !== currentInstrument) {
                    $('.' + arr[1]).css('display', 'none');
                } else {
                    $('.' + arr[1]).css('display', 'inherit');
                }
                if (data[index].history === true) {
                    if ($(this).find('.rate-circle-group').length < 1) {
                        var circle_group = d3.select(this).append('g').attr('class', 'rate-circle-group ' + data[index].name).attr('idbet', data[index].name);
                        createHistoryTooltip(circle_group, data[index].type, data[index].rate, data[index].values[0].timestamp);
                        if (data[index].lose == true) {
                            circle_group.append("circle").attr('class', 'circle-rate-loading history-point-lose').attr('r', 23).attr('stroke-dasharray', 10).attr('stroke-dashoffset', 0);
                            circle_group.append("circle").attr('class', 'circle-rate history-point-lose').attr('r', 23);
                            circle_group.append("text").attr('class', 'rate').text('PROFIT').attr('y', 1).style('visibility', 'visible');
                            circle_group.append("text").attr('class', 'rate').text('$' + 0).attr('y', 12).style('visibility', 'visible');
                        }
                        if (data[index].won == true) {
                            circle_group.append("circle").attr('class', 'circle-rate history-point-won').attr('r', 23);
                            circle_group.append("text").attr('class', 'rate').text('PROFIT').attr('y', 1).style('visibility', 'visible');
                            circle_group.append("text").attr('class', 'rate').text('$' + chartSettings.current_rate.toFixed(2)).attr('y', 12).style('visibility', 'visible');
                        }
                        $(this).removeAttr('isDrawingLoading');
                        $(this).attr('isDrawing', true);
                        $(this).find('.circle-rate-loading').css('visibility', 'hidden');

                        var rate_xtxt = $(this).find(".nv-point").attr('cx');
                        var rate_ytxt = $(this).find(".nv-point").attr('cy');
                        $(this).find('.rate-circle-group').attr('transform', 'translate(' + rate_xtxt + ',' + rate_ytxt + ')');
                    }
                    $(this).find(".nv-point").attr('isHistory', true);
                    $(this).find(".nv-point").attr('idBet', data[index].name);

                    if (data[index].lose == true) {
                        $(this).find('.rate-circle-group .circle-rate').attr('class', 'circle-rate history-point-lose').attr('r', 23);
                        $(this).find('.rate-circle-group .circle-rate-loading').remove();
                        $(this).find('.rate:first').text('PROFIT');
                        $(this).find('.rate:last-child').text('$' + 0);
                        $('.' + arr[1] + ' .nv-line').css('display', 'none');
                    }

                    if (data[index].won == true) {
                        $(this).find('.rate-circle-group .circle-rate').attr('class', 'circle-rate history-point-won').attr('r', 23);
                        $(this).find('.rate-circle-group .circle-rate-loading').remove();
                        $(this).find('.rate:first-child').text('PROFIT');
                        $('.' + arr[1] + ' .nv-line').css('display', 'none');
                    }

                    var tooltipContainer = $(this).find('.tooltip-text-container *');
                    d3.select(this).style('cursor', 'pointer');
                    d3.select(this).on('click', function () {
                        if (!tooltipContainer.attr('open')) {
                            tooltipContainer.css('visibility', 'visible').attr('open', true);
                        }
                        else {
                            tooltipContainer.css('visibility', 'hidden').removeAttr('open');
                        }
                    });
                } else {
                    if ($(this).attr('isDrawingLoading')) {
                        $(this).removeAttr('start');
                        if (!$(this).attr('start')) {
                            var loadingCircle = $(this).find('.circle-rate-loading');
                            loadingCircle.css('visibility', 'visible');
                            d3.selectAll('.circle-rate-loading').transition()
                                .attr("stroke-dashoffset", 150)
                                .ease('linear')
                                .duration(2800);
                            $(this).attr('start', true);
                        }
                    }
                    if (!$(this).attr('isDrawingLoading') && !$(this).attr('isDrawing')) {
                        circle_group = d3.select(this).append('g').attr('class', 'rate-circle-group ' + data[index].name).attr('idbet', data[index].name);
                        createHistoryTooltip(circle_group, data[index].type, data[index].rate, data[index].values[0].timestamp);
                        circle_group.append("circle").attr('class', 'circle-rate-loading').attr('r', 23).attr('stroke-dasharray', 10).attr('stroke-dashoffset', 0).style({'visibility': 'hidden'});
                        circle_group.append("circle").attr('class', 'circle-rate').attr('r', 23).style({'visibility': 'hidden'});
                        circle_group.append("text").attr('class', 'rate').text('RETURN').attr('y', 1).style('visibility', 'hidden');
                        circle_group.append("text").attr('class', 'rate').text('$' + chartSettings.current_rate.toFixed(2)).attr('y', 12).style('visibility', 'hidden');

                        // Initially we draw bet circle on the same place where bet button is.
                        rate_xtxt = $(this).find(".nv-point").attr('cx');
                        rate_ytxt = $(this).find(".nv-point").attr('cy');
                        $(this).find('.rate-circle-group').attr('transform', 'translate(' + rate_xtxt + ',' + rate_ytxt + ')');
                    }

                    if (!loadingBet[data[index].key]) {
                        $(this).removeAttr('isDrawingLoading');
                        $(this).attr('isDrawing', true);
                        $(this).find('.circle-rate-loading').css('visibility', 'hidden');
                    } else {
                        $(this).attr('isDrawingLoading', true);
                    }
                }
                var errorContainer = $(this).find('.tooltip-text-container.error');
                if (errorContainer.length) {
                    $(this).css('display', 'none');
                    $('.' + arr[1] + ' .nv-line').css('display', 'none');
                }
            }
        });
    }

    /**
     * @string dataName
     * @float dataRate
     */
    function createWonCircleAnimation(dataName, dataRate) {
        var $currentPointItem = $('.current-pointer-item');
        var groupName,
            tspan1,
            tspan2,
            cx = Number($currentPointItem.attr('cx')),
            cy = Number($currentPointItem.attr('cy')),
            wonContainer;

        $('.' + dataName + ' *').css('visibility', 'hidden');
        wonContainer = d3.select('.nv-lineChart')
            .append('g')
            .attr('class', 'winner-circle-container')
            .attr('transform', 'translate(' + cx + ',' + cy + ')')
            .attr('name', dataName);
        var wonCircle = wonContainer
            .append('circle')
            .attr('r', 10)
            .attr('class', 'win-circle');
        var wonCircleText = wonContainer
            .append('text')
            .attr('class', 'win-text');
        tspan1 = wonCircleText
            .append('tspan')
            .attr('x', 0)
            .attr('y', -2);
        tspan2 = wonCircleText
            .append('tspan')
            .attr('y', 23)
            .attr('x', 0);
        tspan1.text('$' + dataRate.toFixed(2));
        tspan2.text('Return');

        wonCircle
            .transition()
            .attr('r', 90)
            .duration('1500')
            .each("end", function () {
                wonCircleText.style({'visibility': 'visible'});
            });

        wonContainer
            .transition()
            .style('opacity', 1)
            .duration('1500');

        setTimeout(function () {
            wonContainer
                .transition()
                .style('opacity', 0)
                .duration('1500');
            groupName = wonContainer.attr('name');
            $('.' + groupName + ' .circle-rate').css('visibility', 'visible');
            $('.' + groupName + ' .rate').css('visibility', 'visible');
        }, 4000);
    }


    function createHistoryTooltip(circleGroup, dataType, dataRate, dataTimestamp) {
        var tooltipTextContainer = circleGroup.append("g").attr('class', 'tooltip-text-container'),
            tooltipCoords,
            tspan1CoordsY,
            tspan2CoordsY,
            textMinutes,
            textSeconds,
            tspan1,
            tspan2,
            rateDate,
            tooltipText;

        // Display tooltip below of the chart (if bet type == sell).
        if (dataType === 'sell') {
            tooltipCoords = ['-25,55', '-25,20', '-5,20', '0,15', '5,20', '25,20', '25,55'].join(' ');
            tspan1CoordsY = 38;
            tspan2CoordsY = 50;
        }
        else if (dataType === 'buy') {
            tooltipCoords = ['-25,-55', '-25,-20', '-5,-20', '0,-15', '5,-20', '25,-20', '25,-55'].join(' ');
            tspan1CoordsY = -40;
            tspan2CoordsY = -28;
        }

        tooltipTextContainer.append('polygon')
            .attr('points', tooltipCoords)
            .attr('class', 'tooltip-text-rect');
        tooltipText = tooltipTextContainer.append("text")
            .attr('class', 'tooltip-text');
        tspan1 = tooltipText.append('tspan')
            .attr('y', tspan1CoordsY)
            .attr('x', 0);
        tspan2 = tooltipText.append('tspan')
            .attr('y', tspan2CoordsY)
            .attr('x', 0);

        rateDate = new Date(dataTimestamp);
        textMinutes = preflightZero2(rateDate.getMinutes());
        textSeconds = preflightZero2(rateDate.getSeconds());

        tspan1.text(textMinutes + ':' + textSeconds);
        tspan2.text('$' + dataRate);
    }

    function preflightZero2(number) {
        return (Number(number) < 10 ? '0' + number : number);
    }

    function initializeGradients() {
        var defs = d3.select("svg").append("defs");

        defs.append("image")
            .attr("id", "greengrad")
            .attr('href', 'img/new/block-up.png')
            .attr('width', 172)
            .attr('height', 89)
            .attr("x1", "50%")
            .attr("y1", "100%")
            .attr("x2", "50%")
            .attr("y2", "0%");

        var redLinearGradient = defs.append("linearGradient")
            .attr("id", "redgrad")
            .attr("x1", "50%")
            .attr("y1", "100%")
            .attr("x2", "50%")
            .attr("y2", "0%");

        redLinearGradient.append("stop")
            .attr("offset", "0%")
            .style("stop-color", "rgb(234,80,85)")
            .style("stop-opacity", "1");

        redLinearGradient.append("stop")
            .attr("offset", "95%")
            .style("stop-color", "rgb(234,80,85)")
            .style("stop-opacity", "1");

        redLinearGradient.append("stop")
            .attr("offset", "100%")
            .style("stop-color", "rgb(239,153,159)")
            .style("stop-opacity", "1");
    }

    function createInstance() {
        // Firstly we clear svg.
        $("#chart")
            .html('<svg></svg>');
        updateChartSettings();
        buildInvestmentBar();
        initializeGradients();
        $('.currency').text(userClass.getUserCurrency());
        console.log('starting');
        if (nv.render.queue.length != 0) {
            nv.render.queue = [];
        }
        nv.addGraph(function () {
            chart = nv.models.lineChart()
                .margin({left: 47, right: 0, bottom: 60, top: 0})
                .interactive(false)
                .tooltips(false)
                .showYAxis(true)
                .showLegend(false)
                .showXAxis(true)
                .interpolate("step-before")
                .forceY([chartSettings.minY, chartSettings.maxY])
                .yDomain([chartSettings.minY, chartSettings.maxY])
                .forceX([chartSettings.minX, chartSettings.lastX])
                .xDomain([chartSettings.minX, chartSettings.lastX])
                .x(function (d) {
                    // Get current time in second from (-60 to 0).
                    return (d.timestamp - chartSettings.currentPoint.timestamp) / 1000;
                })
                .y(function (d) {
                    return d.currency;
                });
            // Build Y and X axis.
            buildXAxis();
            buildYAxis();

            // Draw chart.
            if (typeof chart !== 'undefined') {
                d3.select('#chart svg')
                    .datum(data)
                    .transition()
                    .duration(800)
                    .ease('cubic-in-out')
                    .call(chart);
            }

            // Work around for less problem with nth-child css property.
            $('.nv-x.nv-axis .tick').each(function () {
                var text = $(this).find('text');
                if (["-00:30", "00:00", "00:30", "01:00"].indexOf(text.text()) >= 0) {
                    text.css({'font-weight': 500, 'fill': '#fff'});
                }
            });

            // We add clip path for our chart.
            var classPath = $(".nv-scatter").attr('class').split(' ').pop().split('-').pop();
            $('.nv-line > g')
                .css("clip-path", "url(#nv-edge-clip-" + classPath + ")");
            buildPointer();
            if (userClass.isLogin()) {
                buildBets();
                FXChart.resizeButton();
            }

            return chart;
        });
    }

    function hideChartLoading() {
        $('.column-game .spinner-container').hide();
    }

    function showErrorMessage(message) {
        hideChartLoading();
        $('#chart').hide();
        $('.column-game .error-message .error-text').html(message);
        $('.column-game .error-message').show();
    }

    function hideErrorMessage() {
        $('.column-game .error-message').hide();
    }

    function getEmitDataKey() {
        var returnData = ['customerBalance_' + brandshort + '_' + customerId, 'time'],
            assetList = allCurrencyList[assetGroup] ? allCurrencyList[assetGroup] : [currentCurrency];

        $.each(assetList, function (id, currency) {
            returnData.push('asset_' + currency.asset.id);
        });

        return returnData;
    }

    /**
     *
     * @param currencyList
     */
    function groupCurrency(currencyList) {
        $.each(currencyList, function (id, asset) {
            /**
             * @param asset.rule.vipgroup
             */
            if (!allCurrencyList[asset.rule.vipgroup]) allCurrencyList[asset.rule.vipgroup] = [];
            allCurrencyList[asset.rule.vipgroup].push(asset);
        });
    }

    /**
     * Trade me update  (bottom line)
     */
    function updateTrade() {

        setInterval(function () {
            var assetList = $('.slim-scroll .currency-data');
            var randAsset = shuffle(assetList).slice(0, 1)[0];

            var trade_list = $('.inner-column.column-game .trades-list .trade');
            var count = trade_list.length;

            var rand_id = getRandomInt(0, count);
            var old_trade = $('.inner-column.column-game .trades-list').find('.trade[count-id=' + rand_id + ']');

            if (old_trade.length != 0) {
                var $pepper = $('.pepper');
                $pepper.next('.trade-data')
                    .css('background', 'url(../img/new/trade-me-green.png) repeat-x')
                    .css('color', '#191d1f');
                $pepper.parent().find('.trade-name');
                $pepper.remove();
            }

            assetList.each(function (id, asset) {
                if ($(this).attr('data-assetId') == $(randAsset).attr('data-assetId')) {
                    var time = getRandTime();
                    var new_trade = old_trade.html('');
                    var row = '<span class="trade-name">' + $(this).find(':first-child').text() + '</span>' +
                        '<span class="time" trade-time="' + time + '"  asset-time="' + $(this).attr('data-assetId') + '">00:' + time + '0</span> <span class="pepper"></span>' +
                        '<div class="trade-data" data-name="'
                        + $(this).find(':first-child').text() + '" asset-id="' + $(this).attr('data-assetId') + '">Trade me!</div>';
                    new_trade.append(row);
                    $('.pepper').next('.trade-data').css('background', 'url(../img/new/trade-me-read.png) repeat-x').css('color', '#fff');
                    return false;
                }
            });
        }, 7000);
    }

    function shuffle(o) {
        for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) {
        }
        return o;
    }

    /**
     * Trade me buttons initialization (bottom line)
     */
    function showBottomCurrencyList() {
        var assetList = $('.slim-scroll .currency-data');
        var randomizedAssetList = shuffle(assetList);
        var firstAsset = randomizedAssetList.slice(0, 1)[0];

        var trade_me = $('.inner-column.column-game .trades-list');

        var i = 1;
        trade_me.html("");

        //adding first tradeMe item to get it width
        addTradeMeBlock(trade_me, $(firstAsset), i);
        i++;

        //calculating amount of items, that can fit container
        var containerWidth = trade_me[0].getBoundingClientRect().width - (trade_me.outerWidth() - trade_me.width());
        var tradeItem = $('.trade').first();
        var elementWidth = tradeItem[0].getBoundingClientRect().width + (tradeItem.outerWidth(true) - tradeItem.width());
        var availableElements = Math.floor(containerWidth / elementWidth);
        availableElements = availableElements <= maxTradeMeAmount ? availableElements : maxTradeMeAmount;

        //filling last items of trade me block
        var randAsset = randomizedAssetList.slice(1, availableElements);
        randAsset.each(function () {
            addTradeMeBlock(trade_me, $(this), i);
            i++;
        });

        $(document).off('click', 'div.trade-data')
            .on('click', 'div.trade-data', function (e) {
                e.preventDefault();
                console.log('click');
                var id = $(this).attr('asset-id');
                var order = getRandomInt(1, 2);
                var time = $('span.time[asset-time=' + id + ']').attr('trade-time');
                $('div[data-assetId=' + id + ']').click();

                setTimeout(function () {
                    submitRequest('id' + time + '0' + order);
                }, 2000);
            });
    }

    function addTradeMeBlock(trade_me, asset, i) {
        var time = getRandTime();
        var trade = $('<div  count-id="' + i + '" ></div>');
        trade.addClass("trade");
        trade.append('<span class="trade-name">' + asset.find(':first-child').text() + '</span>' +
            '<span class="time" trade-time="' + time + '"  asset-time="' + asset.attr('data-assetId') + '">00:' + time + '0</span>' +
            '<div class="trade-data" data-name="'
            + asset.find(':first-child').text() + '" asset-id="' + asset.attr('data-assetId') + '">Trade me!</div>'
        );
        trade_me.append(trade);

    }

    /**
     *
     */
    function reinitializeTradeMeButtons() {
        $('.trade').remove();
        showBottomCurrencyList();
    }

    function submitRequest(buttonId) {
        if (document.getElementById(buttonId) == null
            || document.getElementById(buttonId) == undefined) {
            return;
        }
        if (document.getElementById(buttonId).dispatchEvent) {
            var e = document.createEvent("MouseEvents");
            e.initEvent("click", true, true);
            document.getElementById(buttonId).dispatchEvent(e);
        } else {
            document.getElementById(buttonId).click();
        }
    }

    function getRandTime() {
        var randomArr = [3], trackingArr = [6],
            targetCount = 1, currentCount = 0,
            min = 3, max = 6,
            rnd;

        while (currentCount < targetCount) {
            rnd = Math.floor(Math.random() * (max - min + 1)) + min;
            if (trackingArr[0] == rnd) {
                trackingArr[rnd] = rnd;
                randomArr[currentCount] = rnd;
                currentCount += 1;
            } else if (randomArr[0] == rnd) {
                trackingArr[rnd] = rnd;
                randomArr[currentCount] = rnd;
                currentCount += 1;
            }
        }

        /**
         * @param userData.broker
         */
        if (checkBrockerBets30(userData.broker)) {
            return randomArr;
        } else {
            return 6;
        }
    }

    /**
     * Left block
     * @param currencyList
     */
    function showCurrencyList(currencyList) {
        currencyList = currencyList ? currencyList : [currentCurrency];
        var container = $('.inner-column.column-currency .slim-scroll');

        container.html("");

        $.each(currencyList, function (id, currency) {
            count++;
            var row = $('<div></div>');

            row.addClass("row currency-data " + currency.asset.name.replace('/', '-'));
            row.attr('data-assetId', currency.asset.id);
            row.append('<div class="col-xs-6 text-left currency-name">' + currency.asset.name + '</div>' +
                '<div class="col-xs-6 pull-right currency-value ' + currency.asset.name + ' text-right"></div>');
            container.append(row);
        });

        $(document).on('click', '.row.currency-data:not(.disabled)', function () {
            countDecimal = 0;
            minPoint = [];
            maxPoint = [];
            firstPoint = 0;
            defaultCurrencyID = $(this).attr('data-assetId');
            $.assetId = $(this).attr('data-assetId');
            currentInstrument = $(this).find('.currency-name').text();

            $('.current-currency-name').html(currentInstrument);
            $('.current-currency-value').html($(this).find('.currency-value').html());

            // hide chart and show loading spinner while new data is not loaded
            $('#chart').html("");
            restart = true;
            FXChart.showChartLoading();

        });
    }


    function resizeButton() {
        var wrap = $(document.getElementsByClassName('nv-wrap')).find('rect')[0].getBoundingClientRect();
        var Y = 0;
        var textY2 = 110;
        var $id302 = $('#id302');
        var $id301 = $('#id301');
        if (wrap.height < 380) {
            var bets = $(document.getElementsByClassName('bets'))[0].getBoundingClientRect();

            var pointer = $id301.attr('transform');
            var transform1 = pointer.split(',');
            if (transform1.length == 1) {
                transform1 = pointer.split(' ');
            }
            Y = Math.abs(parseFloat((transform1[1])));

            pointer = $id302.attr('transform');
            var transform = pointer.split(',');
            if (transform.length == 1) {
                transform = pointer.split(' ');
            }
            textY2 = parseFloat((transform[1]));

            var height = wrap.height;
            var heightButton = (height - 12) / 2 - 6;
            var marginY = (heightButton / 2) + Y;
            var marginY2 = textY2 - (heightButton / 2);
            if (heightButton >= 185) {
                Y = 0;
                marginY = 45;
                marginY2 = 110;
                heightButton = 185;
            }
        }
        else {
            heightButton = 185;
            marginY = 90;
            marginY2 = 110;
            Y = 0;
        }

        $id301.find('image').attr('height', heightButton).attr('y', Y);
        $id301.find('.bet-text-title').attr('y', marginY);
        $id301.find(".bet-txt.bet-txt30-1").attr('y', marginY + 20);

        var $id601 = $('#id601');
        $id601.find('image').attr('height', heightButton).attr('y', Y);
        $id601.find('.bet-text-title').attr('y', marginY);
        $id601.find('.bet-txt.bet-txt60-1').attr('y', marginY + 20);

        $id302.find('image').attr('height', heightButton);
        $id302.find('.bet-text-title').attr('y', marginY2 - 20);
        $id302.find('.bet-txt.bet-txt30-2').attr('y', marginY2);

        var $id602 = $('#id602');
        $id602.find('image').attr('height', heightButton);
        $id602.find('.bet-text-title').attr('y', marginY2 - 20);
        $id602.find('.bet-txt.bet-txt60-2').attr('y', marginY2);

    }

    function getOption(callback) {
        /**
         *
         * @param currencyData
         * @param currencyData.Options
         * @param currencyData.status
         * @param currencyData.errors
         */
        var getRulesCallback = function (currencyData) {

            if (currencyData.status == 'OK') {
                $('#preloader-on').css('display', 'none');
                groupCurrency(currencyData.Options);
                showCurrencyList(allCurrencyList[assetGroup]);
                showBottomCurrencyList(allCurrencyList[assetGroup]);
                updateTrade(allCurrencyList[assetGroup]);
                instruments = [];
                currentCurrency = currencyData.Options[0];
                FXChart.setCurrentCurrencyData();

                callback();
            } else {
                if (currencyData.errors[0].message == 'noRules') {
                    $('#errorModal').find('.modal-body').html('');
                    validateClass.showNoResultMessage('Dear customer, system available between Monday 8 am till Friday 10 pm.');
                }

                if (currencyData.errors[0].message == 'limitExceeded') {
                    integrate.getRules(userClass.getData(), getRulesCallback);
                }

                if (currencyData.errors[0].message == 'sessionExpired') {
                    userClass.logout();
                }


            }
        };

        integrate.getRules(userClass.getData(), getRulesCallback);

    }

    return {
        setResize: function (resizeFlag) {
            chartSettings.resize = resizeFlag;
            chartSettings.resizePointer = resizeFlag;
        },
        initHistoryData: initHistoryData,
        getEmitDataKey: getEmitDataKey,
        drawChart: function (chartPoint, auto) {
            chartPoint = typeof chartPoint !== 'undefined' ? chartPoint : chartSettings.currentPoint;
            auto = typeof auto !== 'undefined' ? auto : false;

            if (restart) {
                initHistoryData();
                return;
            }

            var dataLength = chartSettings.chartPoints.values.length;
            if (!chartPoint || !chartSettings.chartPoints.values[dataLength - 1]) {
                return;
            }

            // As we start interval to avoid "chart's hangs up"(when socket send data very seldom),
            // we need to change 'fake' interval points with real data.
            if (dataLength > 0 && !auto && chartPoint) {
                while (chartPoint.timestamp <= chartSettings.chartPoints.values[chartSettings.chartPoints.values.length - 1].timestamp) {
                    chartSettings.chartPoints.values.pop();
                }
            }
            chartSettings.chartPoints.values.push(chartPoint);

            chartSettings.currentPoint = chartPoint;

            // Check if chart exist already.
            if (typeof chart !== 'undefined') {
                chart = redrawInstance(chartSettings.chartPoints.values);
            }
        },
        getInstrumentsList: function (callback) {
            $.subscribers = callback;
            if (instruments.length > 0) {
                callback();
                return;
            }

            if (userClass.isLogin()) {

                setInterval(
                    function () {
                        if (!$.cookie('user_time')) {
                            userClass.userSessionExpired();
                        }
                    }, 60 * 1000);

                if (!$.cookie('option')) {
                    getOption(callback);
                } else {
                    $('#preloader-on').css('display', 'block');
                }

                setTimeout(function () {
                    userClass.setDepositCountry()
                }, 3000);
                setTimeout(function () {
                    userClass.getCardsUser()
                }, 5000);
                userClass.setBalance($('#balance').text());
            } else {
                currentCurrency = {
                    asset: {
                        id: defaultCurrencyID,
                        name: currencyText
                    },
                    rule: {
                        amountRange: '5,10,25,50,100'
                    }
                };
                callback();
            }
        },
        setCurrentCurrencyData: function () {
            defaultCurrencyID = currentCurrency.asset.id;
            currentInstrument = currentCurrency.asset.name;
        },
        showChartLoading: function () {
            $('#chart').hide();
            $('.column-game .spinner-container').show();
        },
        showErrorMessage: showErrorMessage,
        updateTrade: updateTrade,
        getOption: getOption,
        resizeButton: resizeButton,
        hideErrorMessage: hideErrorMessage,
        reinitializeTradeMeButtons: reinitializeTradeMeButtons
    };
})
($);