function Positions() {
    var integrate = new Integrate(),
        validate = new Validate(),
        userData = userClass.getData(),
        positionsList = [],
        positionsPopupTemplate = '<tr>' +
            '<td class="name">%name%</td>' +
            '<td class="start-date">%startDate%</td>' +
            '<td class="end-date">%endDate%</td>' +
            '<td class="direction">%direction%</td>' +
            '<td class="amount">%amount%</td>' +
            '<td class="entry-rate">%entryRate%</td>' +
            '<td class="end-rate">%endRate%</td>' +
            '</tr>',
        positionData = {
            id: userData.id,
            token: userData.token,
            assetId: '',
            expireIn: '',
            position: '',
            product: 'sixtySeconds',
            amount: ''
        };

    function setAssetId(assetId) {
        positionData.assetId = assetId;
    }

    function setAmount(amount) {
        positionData.amount = amount;
    }

    function openPosition(position, expireIn, callback, error) {
        positionData.position = position;
        positionData.expireIn = expireIn;
        positionData.asset_name = $('.current-currency-name').text();

        integrate.postData('api/position/open', positionData, callback, error);
    }

    function getPosition(uuid) {
        var getPositionData = userData;
        getPositionData.product = 'sixtySeconds';
        var position_id = positionsList[uuid].data_id;

        if (position_id != undefined) {
            integrate.postData('/api/position/get/' + positionsList[uuid].data_id, getPositionData, function (data) {
                if (data.Positions[0].status == 'open') {
                    var isWon;
                    if (positionsList[uuid].data_position == 'call') {
                        isWon = currentTradeValue > positionsList[uuid].data_rate;
                    } else {
                        isWon = currentTradeValue < positionsList[uuid].data_rate;
                    }
                    positionsList[uuid].data_status = isWon ? 'won' : 'lost';

                } else {
                    positionsList[uuid].data_status = data.Positions[0].status;
                }

                if (data.errors[0].message == 'sessionExpired') {
                    userClass.userSessionExpired();
                }
            });
        }

    }

    function getPositionsByStatus(status, spinner) {
        var getPositionData = userData;
        getPositionData.product = 'sixtySeconds';
        getPositionData.status = status;

        integrate.postData('api/position/get', getPositionData, function (data) {
            if (data.status == 'OK') {
                showPositionsAtPopup(status, data.Positions);
            } else {
                if (data.errors[0].message == 'sessionExpired') {
                    userClass.userSessionExpired();
                } else {
                    validate.showMessage(data.errors[0].localizedMessage.l());
                }
            }

            spinner.css('display', 'none');
        })
    }


    function getPositionsForPagination(current, status, spinner, first_result, max_result) {
        var getPositionData = userData;
        getPositionData.first_result = first_result;
        getPositionData.max_result = max_result;
        getPositionData.status = status;

        integrate.postData('api/position/get/pagination', getPositionData, function (data) {
            if (data.status == 'OK') {
                showPositionsAtPopupPagination(current, status, data.pagination);
            } else {
                if (data.errors[0].message == 'sessionExpired') {
                    userClass.logout();
                } else {
                    validate.showMessage(data.errors[0].localizedMessage.l());
                }
            }

            spinner.css('display', 'none');
        })
    }

    function getPagination(current, count) {
        var container = '';
        var pagination = $('#positionsModal .pagination').html('');
        var page = Math.ceil(count / 10);
        var pager = '';

        for (var i = 1; i <= page; i++) {
            pager += '<span page=' + i + ' class="pagination-button number '
                + (i == 1 ? " first " : "")
                + (i == page ? " last " : "")
                + (i == current ? " active" : "") + '">' + i + '</span>';
        }
        container += ' <span id="first" class="pagination-button">&laquo; First</span>' +
            '<span id="prev" class="pagination-button">&lt;</span>'
            + pager +
            '<span id="next" class="pagination-button">&gt;</span>' +
            '<span id="last" class="pagination-button">Last &raquo;</span>';

        pagination.html(container);
    }


    function showPositionsAtPopupPagination(page, status, positions) {
        var positionListHtml = '';
        var table = $('#' + status + ' .table tbody');
        $(positions.positions).each(function (index, position) {
            var positionHtml = positionsPopupTemplate;

            positionHtml = positionHtml.replace('%name%', position.name)
                .replace('%startDate%', getDateByFormat(position.date))
                .replace('%endDate%', getDateByFormat(position.date))
                .replace('%direction%', '<span class="direction ' + position.position + '">' + position.position.l() + '</span>')
                .replace('%amount%', parseFloat(position.amount))
                .replace('%entryRate%', position.entryRate)
                .replace('%endRate%', position.endRate == '0E-8' ? '-' : position.endRate);

            positionListHtml += positionHtml;
        });

        if (positions.count >= 11) {
            getPagination(page, positions.count);
        }

        table.html('');
        table.html(positionListHtml);
    }

    function showPositionsAtPopup(status, positions) {
        var positionListHtml = '';
        var table = $('#' + status + ' .table tbody');
        $(positions).each(function (index, position) {
            var positionHtml = positionsPopupTemplate;

            positionHtml = positionHtml.replace('%name%', position.name)
                .replace('%startDate%', getDateByFormat(position.date))
                .replace('%endDate%', getDateByFormat(position.date))
                .replace('%direction%', '<span class="direction ' + position.position + '">' + position.position.l() + '</span>')
                .replace('%amount%', parseFloat(position.amount))
                .replace('%entryRate%', position.entryRate)
                .replace('%endRate%', position.endRate == '0E-8' ? '-' : position.endRate);

            positionListHtml += positionHtml;
        });

        table.html('');
        table.html(positionListHtml);
    }

    function getDateByFormat(date) {
        var dateObject = new Date(date),
            mNames = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"),
            hour = dateObject.getHours() < 10 ? '0' + dateObject.getHours() : dateObject.getHours(),
            minutes = dateObject.getMinutes() < 10 ? '0' + dateObject.getMinutes() : dateObject.getMinutes();

        return dateObject.getDate() + ' ' + mNames[dateObject.getMonth()].l() + ' ' + dateObject.getFullYear() + ' '
            + ' ' + hour + ':' + minutes + ':' + dateObject.getSeconds();
    }

    return {
        setAssetId: setAssetId,
        openPosition: openPosition,
        getPosition: getPosition,
        positionsList: positionsList,
        getPositionsForPagination: getPositionsForPagination,
        setAmount: setAmount
    };
};