var instruments = [];
var currentInstrument = currencyText;
var restart = true;
var bets = {};
var isSocial;
var userClass = new User();
var validateClass = new Validate();
var positionsClass = new Positions();
var integrateObject = new Integrate();


var templates = {
    game: JST['app/templates/game.us'],
    connect_modal: JST['app/templates/login_popup.us'],
    registration_modal: JST['app/templates/registration_popup.us'],
    deposit_modal: JST['app/templates/deposit_popup.us'],
    error_modal: JST['app/templates/error_popup.us'],
    success_modal: JST['app/templates/success_popup.us'],
    about_modal: JST['app/templates/about_popup.us'],
    regulation_popup: JST['app/templates/regulation_popup.us'],
    positions_popup: JST['app/templates/positions_popup.us'],
    withdrawals_popup: JST['app/templates/withdrawals_popup.us'],
    profile_popup: JST['app/templates/profile_popup.us']
};

window.game = function () {

    $('.view').append(templates.connect_modal);
    userClass.initRegisterButton();
    userClass.initLogoutButton();

    var subscribe = function () {
        console.log("Subscribing");

        FXChart.initHistoryData();
        function request_assets() {
            console.log('connect');
            if ($.disconnectServer == true) {
                FXChart.getOption($.subscribers);
                $.disconnectServer = false;
            }
            $.spotStream.emit('add', FXChart.getEmitDataKey());
        }

        // Create new socket connection.
        $.spotStream = io.connect(spotStreamer, {
            'reconnect': true,
            'reconnection delay': 1000
        });

        $.spotStream.emit('add', FXChart.getEmitDataKey());

        //init action update at socket
        $.spotStream.on('update', function (data) {
            if (typeof( data['customerBalance_' + brandshort + '_' + customerId] ) !== 'undefined') {
                var balance = data['customerBalance_' + brandshort + '_' + customerId].balance;
                $('#balance').html(balance);
                if (balance) {
                    userClass.setBalance(balance);
                }

            }
            $.each(Object.keys(data), function (id, asset) {
                var assetId = asset.replace('asset_', '');
                var assetValueText = $('.user-section .currency-data[data-assetid="' + assetId + '"] .text-right'),
                    currentValue = parseFloat(assetValueText.text()),
                    newValue = parseFloat(data[asset].rate);
                assetValueText
                    .removeClass('currency-up')
                    .removeClass('currency-down')
                    .addClass('change')
                    .text(newValue);

                if (currentValue > 0) {
                    assetValueText.addClass(currentValue > newValue ? 'currency-down' : 'currency-up');
                }
            });
            if (typeof( data['asset_' + defaultCurrencyID] ) !== 'undefined') {
                var rate = data['asset_' + defaultCurrencyID].rate.substr(0, 7);
                currentTradeValue = parseFloat(rate);
            }
        });
        $.spotStream.on('connect', request_assets);
        $.spotStream.on('reconnect', request_assets);

        $.spotStream.on('disconnect', function () {
            $.spotStream.disconnect(true);
            $.disconnectServer = true;
            console.log('disconnect client event....');
        });

        $.spotStream.on('error', function (delay, attempt) {

            setTimeout(function () {
                socket.socket.reconnect();
            }, 5000);
            return console.log("Failed to reconnect. Lets try that again in 5 seconds.");
        });
        window.clearInterval($.updateCurrency);
        $.updateCurrency = self.setInterval(function () {
            updateCurrencyData();
        }, 500);

        window.clearInterval($.updateIntarvalChart);
        $.updateIntarvalChart = self.setInterval(function () {
            intervalTrigger();
        }, 500);
    };

    function currentInvest() {
        $('.cub').each(function () {
            if ($('.input-investment').val() == $(this).find('.invest').attr('val')) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

    function updateCurrencyData() {
        $('.user-section .currency-data .text-right:not(.change)')
            .removeClass('currency-up')
            .removeClass('currency-down');

        $('.user-section .currency-data .text-right.change').removeClass('change');
    }

    function intervalTrigger() {
        lastItemTime += 500;

        var currentPoint = {
            instrument: currencyText,
            currency: currentTradeValue,
            timestamp: lastItemTime
        };

        currentInvest();
        FXChart.drawChart(currentPoint, true);
    }

    FXChart.getInstrumentsList(subscribe);


    window.addEventListener("resize", function () {
        FXChart.setResize(true);
        resizeTutorialModal();
        console.log("Resizing");
    });

    $(window).bind('orientationchange', function (event) {
        FXChart.setResize(true);
    });

    console.log(new Date() + " Start connecting to socket");
};

function resizeTutorialModal() {

    var modal = $('#tutorialModal .modal-dialog');
    var height = modal.outerHeight();
    var width = modal.outerWidth();
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();

    var hRatio = windowHeight / height;
    var vRatio = windowWidth / width;

    var height_result = height * 0.9 * vRatio;
    if (vRatio <= hRatio) {
        modal.attr('style', height_result != 0 ? 'height:' + height * 0.9 * vRatio + 'px!important;' : '' + ' width:' + width * 0.9 * vRatio + '!important;');
    } else {
        modal.attr('style', 'width:' + width * 0.9 * hRatio + 'px!important; height:' + height * 0.9 * hRatio + '!important;');
    }

}
$(document).ready(function () {
    var viewEl = $('.view');
    isSocial = (utils.getUrlParameter('social') !== 'false');
    //append element at folder "templates"
    viewEl.html(templates.game);
    menuController();
    viewEl.append(templates.deposit_modal);
    viewEl.append(templates.error_modal);
    viewEl.append(templates.success_modal);
    viewEl.append(templates.about_modal);
    viewEl.append(templates.regulation_popup);
    viewEl.append(templates.positions_popup);
    viewEl.append(templates.withdrawals_popup);
    viewEl.append(templates.profile_popup);
    $('#tutorialModal').css('z-index', 99999999);
    $('#welcomeModal').css('z-index', 99999999);

    $(document).on('click', function () {
        var date = new Date();
        var minutes = 30;
        date.setTime(date.getTime() + (minutes * 60 * 1000));
        $.cookie('user_time', 1, {expires: date})
    });

    var resizeTimer;

    $(window).on('resize', function (e) {
        FXChart.setResize(true);
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            console.log('resizing Button');
            FXChart.resizeButton();
        }, 500);
    });

    utils.initializeInformationSection();
    utils.initializeDepositButton();
    utils.initializeTradersChoiceCheckbox();
    utils.initializeSlimScroll();
    // Initialize game clock.
    setInterval(function () {
        if ($.fn.clock) {
            $('.clock-wrapper').html($.fn.clock());
        }
    }, 1000);

    $(document).ready(function () {
        $("#validation-date-input-id").mask("99/9999");

    });
    error.initErrorPopup(utils.getUrlParameter('debug'));
    success.initSuccessPopup(utils.getUrlParameter('debug'));


    var errorCallback = function () {
        var timeToRestart = 10;

        function updateTime() {
            FXChart.showErrorMessage("connect_error".l_with_args({seconds: timeToRestart}));

            if (timeToRestart === 0) {
                integrateObject.checkServer(successCallback, errorCallback);
            } else {
                setTimeout(updateTime, 1000);
            }

            timeToRestart--;
        }

        updateTime();
    };
    var successCallback = function () {

        userClass.initData(function () {

            userClass.initLoginPopup(templates.connect_modal);
            userClass.initRegistrationPopup(templates.registration_modal);

            userClass.init(window.game);
            $('.random-percent').css('visibility', 'hidden');
        });
    };

    /**
     * if server worked - start game
     */
    integrateObject.checkServer(successCallback, errorCallback);

    var timeout = null;
    window.addEventListener("resize", function () {
        if (timeout !== null) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            FXChart.reinitializeTradeMeButtons();
            timeout = null;
        }, 500);
    });
});
