function User() {
    var integrate = new Integrate(),
        data = {
            id: $.cookie('user_id'),
            token: $.cookie('user_token')
        },
        registerCountryList = [],
        depositCountryList = [],
        userCreditCards = [],
        userData = {},
        uid;

    /**
     * @function callback
     */
    function init(callback) {
        if (!userData) {
            userSessionExpired();
        }

        if (!Object.keys(userData).length) {
            $.cookie('user_id', null);
            $.cookie('user_token', null);
            showAnonymous();
            translateAll();
        }
        callback();
    }

    /**
     * set country list
     * @param callback
     */
    function setRegisterCountry(callback) {
        integrateObject.postData('api/countries/get', {}, function (data) {
            insertCountryList(data, callback);
        });
    }

    /**
     * @object countryJsonData
     * @function callback
     */
    function insertDepositCountryList() {
        integrateObject.postData('api/countries/deposit', {id: data.id, token: data.token}, function (countryJsonData) {
            if (countryJsonData.status == 'OK') {
                depositCountryList = countryJsonData.countries;
                $.each(countryJsonData.countries, function (id, country) {
                    if (country) {
                        $('#depositModal select#country_deposit').append('<option value="' + country.id + '" data-prefix="+' + country.prefix + '">' + country.name + '</option>');
                        $('#videoDepositModal select#country_deposit').append('<option value="' + country.id + '" data-prefix="+' + country.prefix + '">' + country.name + '</option>');
                    }
                });
            } else {
                if (countryJsonData.errors[0].message == 'sessionExpired') {
                    userClass.userSessionExpired();
                }
            }
        });

    }

    /**
     * @object countryJsonData
     * @function callback
     */
    function insertCountryList(countryJsonData, callback) {
        if (countryJsonData.status == 'OK') {
            registerCountryList = countryJsonData.countries;
            getUserData(callback);
        } else {
            console.log('Error: get country with error connection_status = ' + countryList.status.connection_status +
                ', error = ' + countryList.status.errors.error);
            callback();
        }
    }

    function phoneCode(country_id) {
        var phone_code = 0;
        $.each(registerCountryList, function (id, country) {
            if (country.id == country_id) {
                phone_code = country.code;
                $('input#phone_number_pop_up').val(phone_code);
                $('input#customer_phone').val(phone_code);
            }
        });
    }

    /**
     * show data if user not login
     */
    function showAnonymous() {
        $('.user-section .anonymous').show();
        $('.user-section .expanded').hide().width(68);
        $('.game-section').addClass('anonymous-user-login');
        $('#connectModal').modal({backdrop: 'static', keyboard: false});
    }

    /****** user action ******/
    /**
     * login user by form
     * @object loginForm
     */
    function loginUserByForm(loginForm) {
        if (validate.formValidate(loginForm)) {
            loginUser(loginForm.serialize());
        } else {
            $('p.error-message', loginForm).show(400);
        }
    }

    /**
     * login user by email and password
     * @param data
     */
    function loginUser(data) {
        integrate.postData('api/auth/login', data, loginUserCallback);
    }

    /**
     * action after user login
     * @object returnData
     */
    function loginUserCallback(returnData) {
        if (returnData.status == 'ERROR') {
            validate.showMessage(returnData.errors[0].localizedMessage.l());
        } else {
            $.cookie('user_id', returnData.login.id);
            $.cookie('user_token', returnData.login.token);
            $.cookie('uid', returnData.login.uid);
            window.location.reload();
        }
    }

    function showWelcome() {
        if ($.cookie('index') == 'new') {
            $('#welcomeModal').modal();
            $.cookie('index', null);
        }
    }

    function showVideo() {
        if ($.cookie('video') == 'open') {
            openUserVideoDepositModal();
            $.cookie('video', null);
        }
    }

    /**
     * registration user
     * @object registrationForm
     */
    function registrationUser(registrationForm) {
        if (validate.validateRegistrationForm(registrationForm)) {
            var offers = [
                {
                    offer_code: getUrlVars()["a_aid"] ? getUrlVars()["a_aid"] : '',
                    offer_type: getUrlVars()["a_aid"] ? 'a_aid' : ''
                },
                {
                    offer_code: getUrlVars()["a_bid"] ? getUrlVars()["a_bid"] : '',
                    offer_type: getUrlVars()["a_bid"] ? 'a_bid' : ''
                },
                {
                    offer_code: getUrlVars()["a_cid"] ? getUrlVars()["a_cid"] : '',
                    offer_type: getUrlVars()["a_cid"] ? 'a_cid' : ''
                }
            ];

            var valid = false;
            for (var i = 0, resultArr = []; i < offers.length; i++) {
                $.each(offers[i], function (id, item) {
                    if (item != '') {
                        valid = true;
                    }
                });
                if (valid) {
                    resultArr.unshift(offers[i]);
                    valid = false;
                }
            }

            var data = {
                first_name: $('input[name=first_name]').val(),
                last_name: $('input[name=last_name]').val(),
                email: $('input[name=reg_email]').val(),
                password: $('input[name=reg_password]').val(),
                cell_number: $('input[name=phone_number]').val().replace('+', ''),
                phone_number: $('input[name=phone_number]').val().replace('+', ''),
                reg_country: $('select[name=reg_country]').val(),
                offer: resultArr,
                birth: '1990-01-01'
            };

            integrate.postJsonData('api/customer/add', data, registrationUserCallBack);
        } else {
            $('p.error-message', registrationForm).show(400);
        }
    }

    /**
     * get credit cards user
     * @object registrationForm
     */
    function getCardsUser() {
        integrate.postData('api/creditcard/view',
            {
                id: $.cookie('user_id'),
                token: $.cookie('user_token')
            },
            function (creditCardData) {
                if (creditCardData.status == 'OK') {
                    userCreditCards = creditCardData.cards;
                    $(".new-card").css('display', 'inline-block');
                    $(".exist-card").css('display', 'inline-block');
                    addCreditCardListToSelect($('#depositModal select#credit_card'), creditCardData.cards);
                    addCreditCardListToSelectWinsdrowals($('#withdrawModal select#credit_card_list'), creditCardData.cards);
                } else if (creditCardData.status == 'ERROR') {
                    $(".new-card").css('display', 'none');
                    $(".exist-card").css('display', 'none');

                    if (creditCardData.errors[0].message == 'limitExceeded') {
                        getCardsUser();
                        return true;
                    }

                    if (creditCardData.errors[0].message == 'sessionExpired') {
                        userClass.userSessionExpired();
                    }
                }
            });
    }

    function getDateByFormat(date) {
        var dateObject = new Date(date),
            mNames = ["Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec"],
            hour = dateObject.getHours() < 10 ? '0' + dateObject.getHours() : dateObject.getHours(),
            minutes = dateObject.getMinutes() < 10 ? '0' + dateObject.getMinutes() : dateObject.getMinutes();

        return dateObject.getDate() + ' ' + mNames[dateObject.getMonth()].l() + ' ' + dateObject.getFullYear() + ' '
            + ' ' + hour + ':' + minutes + ':' + dateObject.getSeconds();
    }

    /**
     * get withdrawals user
     * @object registrationForm
     */
    function getWithdrawalsUser() {

        integrate.postData('api/withdrawals/get',
            {
                id: $.cookie('user_id'),
                token: $.cookie('user_token')
            },
            function (data) {
                if (data.status == 'OK') {
                    $('#approved .table tbody').html('');
                    $('#pending .table tbody').html('');
                    $('#canceled .table tbody').html('');
                    $.each(data.withdrawals, function (id, total) {
                        if (total.operation_status == 'PENDING') {
                            $('#pending .table tbody').append(
                                '<tr><td>Credit card</td><td>' + total.amount + '</td><td>' + getDateByFormat(total.request_time) + '</td></tr>'
                            );
                        }

                        if (total.operation_status == 'APPROVED') {
                            $('#approved .table tbody').append(
                                '<tr><td>Credit card</td><td>' + total.amount + '</td><td>' + getDateByFormat(total.request_time) + '</td></tr>'
                            );
                        }

                        if (total.operation_status == 'CANCELED') {
                            $('#canceled .table tbody').append(
                                '<tr><td>Credit card</td><td>' + total.amount + '</td><td>' + getDateByFormat(total.request_time) + '</td></tr>'
                            );
                        }
                    });

                } else if (data.status == 'ERROR') {
                    if (data.errors[0].message == 'sessionExpired') {
                        userClass.userSessionExpired();
                    } else {
                        validate.showMessage(data.errors[0].localizedMessage.l());
                    }
                }
            });
    }

    /**
     * set withdrawals  user
     * @object registrationForm
     */
    function withdrawalsUser(withdrawalsForm) {
        if (validate.validateWithdrawalsForm(withdrawalsForm)) {
            var val = $('#credit_card_list').val().split('*');

            var data_exist_card = {
                id: $.cookie('user_id'),
                token: $.cookie('user_token'),
                card_id: val[1],
                fund_id: val[0],
                operation_type: 'creditCard',
                amount: $('#withdrawModal input[name="Amount"]').val()
            };
            $('#withdrawModal .spinner-container').css('display', 'block');

            integrate.postJsonData('api/withdrawals/add', data_exist_card, withdrawalsUserCallBack);
        } else {
            $('p.error-message', withdrawalsForm).show(400);
        }
    }

    /**
     * registration user
     * @object registrationForm
     */
    function depositUser(depositForm) {

        var exist_card = $('#credit_card', depositForm).val();

        if ($('#exist-card').css('display') == 'block') {
            if (validate.validateExistCardDepositForm(depositForm)) {
                var data_exist_card = {
                    id: $.cookie('user_id'),
                    token: $.cookie('user_token'),
                    fundId: exist_card,
                    amount: $('.amount-input', depositForm).val()

                };

                $.amount = $('.amount-input', depositForm).val();

                depositForm.prev().find('.spinner-container').css('display', 'block');

                integrate.postDepositData('api/deposit/add/exist', data_exist_card);

                setTimeout(function () {
                    $('#close_deposit .spinner-container').css('display', 'none');
                    $('#videoDepositModal .spinner-container').css('display', 'none');
                    $('#close_deposit').click();
                    $('#videoDepositModal #close_deposit').click();
                    showMessage('Thank you! Your deposit is in process. Please check your balance soon. For inquiries contact: support@test.com');
                }, 5000);

            } else {
                $('p.error-message', depositForm).show(400);
            }
        } else if ($('#new-card').css('display') == 'block') {

            var cardNumber = $(".card_number").map(function () {
                return $(this).val();
            }).get();

            if (validate.validateDepositForm(depositForm)) {
                var date = $('#validation-date-input-id', depositForm).val();
                var number = cardNumber.join().replace(new RegExp(',', 'g'), '');
                var data = {
                    id: $.cookie('user_id'),
                    token: $.cookie('user_token'),
                    method: 'creditCard',
                    cardType: '1',
                    cardNum: number,
                    ExpMonth: $('#expiresMonth', depositForm).val(),
                    ExpYear: $('#expiresYear', depositForm).val(),
                    FirstName: $('#first-name-on-card-input', depositForm).val(),
                    LastName: $('#last-name-on-card-input', depositForm).val(),
                    Address: $('#deposit-address-input', depositForm).val(),
                    City: $('#deposit-city-input', depositForm).val(),
                    Country: $('#country_deposit', depositForm).val(),
                    State: 'MINSK',
                    Phone: $('#deposit-phone-input', depositForm).val(),
                    currency: userData.currency,
                    amount: $('#new-card input[name="Amount"]', depositForm).val(),
                    fundId: '-1',
                    'CVV2/PIN': $('#cvv', depositForm).val(),
                    postCode: $('#postCode-input', depositForm).val()
                };

                $.amount = $('#new-card input[name="Amount"]', depositForm).val();

                depositForm.prev().find('.spinner-container').css('display', 'block');

                integrate.postData('api/deposit/add', data);

                setTimeout(function () {
                    $('#close_deposit .spinner-container').css('display', 'none');
                    $('#videoDepositModal .spinner-container').css('display', 'none');
                    $('#close_deposit').click();
                    $('#videoDepositModal #close_deposit').click();
                    showMessage('Thank you! Your deposit is in process. Please check your balance soon. For inquiries contact: support@test.com');
                }, 5000);
            } else {
                $('p.error-message', depositForm).show(400);
            }
        }
    }

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }


    function stopVideo() {
        var videoURL = $('#video-frame').prop('src');
        videoURL = videoURL.replace("?playlist=lsdZEofj2fc&rel=0&amp;showinfo=0&autoplay=1&loop=1&controls=0&modestbranding=1", "");
        $('#video-frame').prop('src', '');
        $('#video-frame').prop('src', videoURL);
    }

    function startVideo() {
        var videoURL = $('#video-frame').prop('src');
        videoURL += "?playlist=lsdZEofj2fc&rel=0&amp;showinfo=0&autoplay=1&loop=1&controls=0&modestbranding=1";
        $('#video-frame').prop('src', videoURL);
    }

    function showMessage(message) {
        return validateClass.showSuccessMessage(message);
    }

    /**
     * action after user register
     * @param withdrawals
     */
    function withdrawalsUserCallBack(withdrawals) {
        if (withdrawals.status == 'OK') {
            $('#withdrawModal').modal('hide');
            validateClass.showSuccessMessage('Your withdrawal request is being processed, Our representatives will contact you  if any further information is required');

        } else {
            if (withdrawals.errors[0].message == 'sessionExpired') {
                userClass.userSessionExpired();
            } else {
                validate.showMessage(withdrawals.errors[0].localizedMessage.l());
            }
        }
        $('#withdrawModal .spinner-container').css('display', 'none');
        exit();
    }

    /**
     * Reset user password
     * @object registrationForm
     */
    function resetUserPassword(resetPasswordForm) {

        if (validate.validateResetPasswordForm(resetPasswordForm)) {
            var data = {
                id: $.cookie('user_id'),
                token: $.cookie('user_token'),
                newpass: $('input#change_pass').val(),
                rnewpass: $('input#confirm_pass').val(),
                oldpass: $('input#old_pass').val()
            };

            integrate.postData('api/customer/password/update', data, resetPassUserCallBack);
        } else {
            $('p.error-message', resetPasswordForm).show(400);
        }
    }


    /**
     * action after user reset password
     * @param resetPassData
     */
    function resetPassUserCallBack(resetPassData) {
        if (resetPassData.status == 'OK') {
            logout();
        } else {
            if (resetPassData.errors[0].message == 'sessionExpired') {
                userClass.userSessionExpired();
            } else {
                validate.showMessage(resetPassData.errors[0].localizedMessage.l());
            }
        }
    }

    /**
     * action after user register
     * @param registrationData
     */
    function registrationUserCallBack(registrationData) {
        if (registrationData.status == 'OK') {
            var loginData = {
                email: $('#registrationModal input[name="reg_email"]').val(),
                password: $('#registrationModal input[name="reg_password"]').val()
            };
            $.cookie('index', 'new');
            loginUser(loginData);
        } else {
            validate.showMessage(registrationData.errors[0].localizedMessage.l());
        }
    }

    /**
     * action after get user cards
     * @param creditCardData
     */
    function getCardsUserCallBack(creditCardData) {
        if (creditCardData.status == 'OK') {
            userCreditCards = creditCardData.cards;
        }
    }

    /**
     * logout user function
     */
    function logout() {
        integrate.postData('api/auth/logout', data, function (data) {
            if (data.logout_status == "OK") {
                $.cookie('user_id', null);
                $.cookie('user_token', null);

                window.location.reload();
            }
        });
    }

    /**
     * get user data by id and token
     * @function callback
     */
    function getUserData(callback) {
        if (isLogin()) {
            integrate.postData('api/customer/info', data, function (userInfo) {
                if (userInfo.status != 'ERROR') {
                    setUserData(userInfo);
                } else {
                    if (userInfo.errors[0].localizedMessage == 'sessionExpired') {
                        userSessionExpired();
                    } else {
                        userData = userInfo.customer;
                    }
                }

                callback();
            }, function (e) {
                userData = {};
                console.log('error for get user data');

                callback();
            })

        } else {
            console.log('user is not login');
            $('.is-login').hide();
            callback();
        }
    }

    /**
     * action after get user data
     * @param userInfo
     */
    function setUserData(userInfo) {
        userData = userInfo.customer;
        console.log('user is login');
        userData['id'] = $.cookie('uid');
        $('.user-section .user-name').text(userData.FirstName + ' ' + userData.LastName);
        $('#welcome-caption').text('Welcome, ' + userData.FirstName + "!");
        var lang = registerCountryList[userData.reg_country] && languageList.indexOf(registerCountryList[userData.reg_country].defaultLang) + 1 ?
            registerCountryList[userData.reg_country].defaultLang : defaultLang;
        userData.accountBalance = userData.accountBalance == null ? 0.00 : userData.accountBalance;
        Globalite.setLang(lang);
        translateAll();
        $('.is-not-login').hide();
        $('#balance').text(userData.accountBalance);
        $('.currency').text(currencySymbolsList[userData.currency]);

        $.cookie('status', userData.regStatus);
        $.cookie('broker', userData.broker);
        $.cookie('email_broker', userData.broker_email != null ? userData.broker_email : ' ');

        var image_name = (userData.broker == 'BDBE' || userData.broker == 'BDBG') ? 'BDB' : userData.broker;

        $('#depositModal .deposit-logo').css('background-image', 'url(../img/new/logo/' + image_name + '.png)');
        $('#videoDepositModal .deposit-logo').css('background-image', 'url(../img/new/logo/' + image_name + '.png)');
        window.userData = userData;
        window.spotStreamer = userData.urls;
        window.spotPlatform = userData.broker_url;
        window.brandshort = userData.brandsort;
        window.customerId = userData.uid;

        setUserCookie(userData);

        if (window.userData != undefined) {
            spotPlatform = window.spotPlatform;
            spotStreamer = window.spotStreamer;
            brandshort = window.brandshort;
            customerId = window.customerId;

            window.clearInterval($.updateCurrency);
            window.clearInterval($.updateIntarvalChart);
            $('svg').html('');
            userClass.init(window.game);
            $('.random-percent').css('visibility', 'hidden');
        }

        $('span.broker').text('powered by: ' + userData.broker);
        showWelcome();
        showVideo();
    }

    function getWeekDay(date) {
        date = date || new Date();
        var days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        var day = date.getDay();

        return days[day];
    }

    function clearUserCookie() {
        $.cookie('brokerUrl', null);
        $.cookie('urls', null);
        $.cookie('brandShort', null);
        $.cookie('assetGroup', null);
        $.cookie('status', null);
        $.cookie('broker', null);
        $.cookie('email_broker', null);
    }

    function setUserCookie(userData) {
        clearUserCookie();

        if (userData.length == 0) {
            return false;
        }

        $.cookie('brokerUrl', userData.broker_url);
        $.cookie('urls', userData.urls);
        $.cookie('brandShort', userData.brandsort);

        $.cookie('assetGroup', userData.assetGroup ? userData.assetGroup : 'Regular');

        $.cookie('status', userData.regStatus);
        $.cookie('broker', userData.broker);
        $.cookie('email_broker', userData.broker_email != null ? userData.broker_email : ' ');
        return true;
    }

    function getUserStatus() {

        var active = false;

        if (userData.regStatus == 'activated') {
            active = true;
        }

        return active;
    }

    function openUserDepositModal() {
        getCardsUser();
        setTimeout(insertDepositCountryList(), 2000);

        setTimeout(function () {
            $('#depositModal').modal({backdrop: 'static', keyboard: false});
            $('#depositModal .visa-mastercard').focus();
        }, 1000);

    }

    function openUserVideoDepositModal() {
        $('#videoDepositModal>.modal-backdrop .fade .in').css('opacity', '1', 'important');
        getCardsUser();
        startVideo();
        setTimeout(insertDepositCountryList(), 2000);
        setTimeout(function () {
            $('#videoDepositModal').modal({backdrop: 'static', keyboard: false});
            $('#videoDepositModal .visa-mastercard').focus();
        }, 3000);
    }

    /**
     * action if user session expired
     */
    function userSessionExpired() {
        data = {
            id: null,
            token: null
        };

        $('.is-login').hide();

        $.cookie('user_id', null);
        $.cookie('user_token', null);
        $.removeCookie('isRegulationModal', {path: '/'});
        $.removeCookie('option', {path: '/'});
        $.removeCookie('email_broker', {path: '/'});
        $.removeCookie('brokerUrl', {path: '/'});
        $.removeCookie('brandShort', {path: '/'});
        $.removeCookie('assetGroup', {path: '/'});
        $.removeCookie('broker', {path: '/'});
        $.removeCookie('uid', {path: '/'});
        $.removeCookie('urls', {path: '/'});
        $.removeCookie('status', {path: '/'});
        window.location.reload();
        console.log('user is not login');
    }

    /**
     * @returns string
     */
    function getUserCurrency() {
        return currencySymbolsList[userData.currency];
    }

    function showUserDataAtPopup() {
        $('#profileModal input[name="email"]').val(userData.email);
        $('#profileModal input[name="name"]').val(userData.FirstName + ' ' + userData.LastName);
        $('#profileModal input[name="phone"]').val(userData.phone_number);

        var userCurrency = '';
        $.each(registerCountryList, function (id, country) {
            if (country) {
                if (country.id == userData.country) {
                    userCurrency = country.name;
                }
            }
        });

        $('#profileModal input[name="country"]').val(userCurrency);

        var dateOfBirth = userData.birthday.split('-');

        $('#profileModal input[name="day"]').val(dateOfBirth[2]);
        $('#profileModal input[name="month"]').val(dateOfBirth[1]);
        $('#profileModal input[name="year"]').val(dateOfBirth[0]);

        $('#profileModal .price').text(getUserCurrency() + userData.accountBalance);
    }

    function setBalance(balance) {
        userData.accountBalance = balance;
        $('#profileModal .price').text(getUserCurrency() + balance ? balance : 0);
        if (parseInt(balance) == 0) {
        } else {
            integrate.postData('api/customer/balance/set', {
                id: $.cookie('user_id'),
                token: $.cookie('user_token'),
                balance: balance
            }, function (data) {
            });
        }

    }

    /************init user templates*************/
    function addCountryListToSelect(select) {
        $.each(registerCountryList, function (id, country) {
            if (country) {
                select.append('<option value="' + country.id + '" data-prefix="+' + country.prefix + '">' + country.name + '</option>');
            }
        });
    }

    function addDepositCountryListToSelect(selecter) {
        $.each(depositCountryList, function (id, country) {
            if (country) {
                selecter.append('<option value="' + country.id + '" data-prefix="+' + country.prefix + '">' + country.name + '</option>');
            }
        });
    }

    function addCreditCardListToSelect(selecter, listCreditCards) {

        selecter.html('');
        selecter.append('<option value selected>Select credit card</option>');
        $.each(listCreditCards, function (id, card) {
            if (card) {
                selecter.append('<option value="' + card.fundId + '"> **** **** **** ' + card.cardNum + '</option>');
            }
        });
    }

    function addCreditCardListToSelectWinsdrowals(selecter, listCreditCards) {

        selecter.html('');
        selecter.append('<option value selected>Select credit card</option>');
        $.each(listCreditCards, function (id, card) {
            if (card) {
                selecter.append('<option value="' + card.fundId + '*' + card.id + '"> **** **** **** ' + card.cardNum + '</option>');
            }
        });
    }

    function initLoginPopup(template) {
        $('.view').append(template);
    }


    function initRegistrationPopup(template) {
        $('.view').append(template);
        addCountryListToSelect($('#registrationModal select[name="reg_country"]'));
        userFormController();

        $(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
    }

    function initRegisterButton() {
        $(document).on('click', 'a.register-button', function (event) {
            event.preventDefault();
            $('#connectModal .modal-backdrop').click();
            $('#registrationModal').modal();
        });
        $(document).on('click', 'a.login-button', function (event) {
            event.preventDefault();
            $('#registrationModal .modal-backdrop').click();
            $('#connectModal').modal({backdrop: 'static', keyboard: false});
        });

        if (isLogin()) {
            $('.user-info').show();
        }
    }

    function initLogoutButton() {
        $(document).on('click', 'a.logout', function () {
            logout();
        });
    }

    var validate = new Validate();

    validate.validateRegistrationForm = function (form) {
        var isValidate = validate.formValidate(form),
            passwordContainer = $('input[name="password"]', form).parents('.row-fluid'),
            password = $('input[name="reg_password"]', form).val(),
            passwordConfirm = $('input[name="confirm_password"]', form).val();

        if (isValidate == true && password != passwordConfirm) {
            validate.showMessage('Passwords not equal');
            isValidate = false;
        }
        return isValidate;
    };

    function isInteger(num) {
        return (num ^ 0) === num;
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    validate.validateDepositForm = function (form) {
        var isValide = true;
        var deposit = $('input.amount-input');

        var cardNumber = $(".card_number", form).map(function () {
            return $(this).val();
        }).get();
        var number = cardNumber.join().replace(new RegExp(',', 'g'), '');
        var cardValidate;


        if ($('#new-card').css('display') == 'block') {
            $('input, select', form.find('#new-card')).each(function () {
                var container = $(this).parent(),
                    value = $(this).val();

                if ($(this).attr('name') == "Amount" && value <= 0) {
                    $(this).parents('.amount').find('p.error-message').text('Below minimum amount');
                    isValide = false;
                } else {
                    $(this).parents('.amount').find('p.error-message').text('');
                    isValide = true;
                }

                if ($(this).attr('required') == 'required' && value.trim().length == 0) {
                    container.find('p.error-message').text($(this).attr('name') + ' is required');
                    isValide = false;
                } else {
                    container.find('p.error-message').text('');

                    if (number.length == 16) {
                        $.each(cardNumber, function (id, data) {
                            var data1 = data[0] * 2;
                            var data2 = data[2] * 2;
                            cardNumber[id] = ( data1.toString().length == 2 ? parseInt(data1.toString()[0]) + parseInt(data1.toString()[1]) : parseInt(data1)) + parseInt(data[1])
                                + (data2.toString().length == 2 ? parseInt(data2.toString()[0]) + parseInt(data2.toString()[1]) : parseInt(data2)) + parseInt(data[3]);
                        });

                        cardValidate = (parseInt(cardNumber[0]) + parseInt(cardNumber[1]) + parseInt(cardNumber[2]) + parseInt(cardNumber[3])) / 10;

                        if (!isInteger(cardValidate) && $.isNumeric(cardValidate)) {
                            $('.card-error', form).text("Card number isn't valid");
                            isValide = false;
                        } else {
                            $('.card-error', form).text('');
                        }

                    } else if (number.length == 14) {
                        $('.card-error', form).text('');
                        isValide = true;
                    } else {
                        $('.card-error', form).text("Card number isn't valid");
                        isValide = false;
                    }

                }
            });
        }


        return isValide;
    };

    validate.validateResetPasswordForm = function (form) {
        var isValide = true;

        $('input, select', form).each(function () {
            var container = $(this).parents('.row-fluid'),
                value = $(this).val();

            if ($(this).attr('required') == 'required' && value.trim().length == 0) {
                container.find('p.error-message').text($(this).attr('name') + ' field is required');
                isValide = false;
            } else {
                container.find('p.error-message').text('');
            }

        });
        return isValide;
    };


    validate.validateExistCardDepositForm = function (form) {
        var isValide = true;
        if ($('#exist-card').css('display') == 'block') {
            $('input, select', '#exist-card').each(function () {
                var container = $(this).parents('.row-fluid'),
                    value = $(this).val();

                if ($(this).attr('required') == 'required' && value.length == 0) {
                    $(this).parents('.row-fluid').find('p.error-message').text($(this).attr('name') + ' field is required');
                    isValide = false;
                } else {
                    $(this).parents('.row-fluid').find('p.error-message').text('');
                }
            });
        }
        return isValide;
    };


    validate.validateWithdrawalsForm = function (form) {

        var deposit = $('input.amount-input');

        var isValide = true;
        $('input, select', form).each(function () {
            var container = $(this).parents('.row-fluid'),
                value = $(this).val();

            if ($(this).attr('required') == 'required' && value.length == 0) {
                $(this).next('p.error-message').text($(this).attr('name') + ' field is required');
                isValide = false;
            } else {
                $(this).next('p.error-message').find('p.error-message').text('');
                if ($(this).hasClass('amount-withdraw') && $(this).val() <= 0) {
                    $(this).next('p.error-message').text('Below minimum amount');
                    isValide = false;
                } else {
                    $(this).next('p.error-message').text('');
                }
            }
        });

        return isValide;
    };

    function isLogin() {
        return data.id && data.token && data.id != 'null' && data.token != 'null';
    }

    return {
        initRegistrationPopup: initRegistrationPopup,
        initData: setRegisterCountry,
        getCardsUser: getCardsUser,
        setDepositCountry: insertDepositCountryList,
        openUserDepositModal: openUserDepositModal,
        initLoginPopup: initLoginPopup,
        initLogoutButton: initLogoutButton,
        loginUser: loginUserByForm,
        initRegisterButton: initRegisterButton,
        registrationUser: registrationUser,
        getUserCurrency: getUserCurrency,
        showUserDataAtPopup: showUserDataAtPopup,
        isLogin: isLogin,
        showMessage: showMessage,
        stopVideo: stopVideo,
        depositUser: depositUser,
        withdrawalsUser: withdrawalsUser,
        phoneCode: phoneCode,
        setBalance: setBalance,
        getWithdrawalsUser: getWithdrawalsUser,
        resetUserPassword: resetUserPassword,
        getData: function () {
            return data;
        },
        init: init,
        userSessionExpired: userSessionExpired,
        logout: logout,
    };
}
